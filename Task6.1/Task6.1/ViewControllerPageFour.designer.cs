// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6
{
	[Register ("ViewControllerPageFour")]
	partial class ViewControllerPageFour
	{
		[Outlet]
		UIKit.UIButton ButtonAlertOKCancelPageFour { get; set; }

		[Outlet]
		UIKit.UIButton ButtonAlertOKCustomCancelPageFour { get; set; }

		[Outlet]
		UIKit.UIButton ButtonAlertOKPageFour { get; set; }

		[Outlet]
		UIKit.UISegmentedControl SegmentedControlPageFour { get; set; }

		[Outlet]
		UIKit.UISlider SliderPageFour { get; set; }

		[Outlet]
		UIKit.UISwitch SwitchPageFour { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ButtonAlertOKCancelPageFour != null) {
				ButtonAlertOKCancelPageFour.Dispose ();
				ButtonAlertOKCancelPageFour = null;
			}

			if (ButtonAlertOKCustomCancelPageFour != null) {
				ButtonAlertOKCustomCancelPageFour.Dispose ();
				ButtonAlertOKCustomCancelPageFour = null;
			}

			if (ButtonAlertOKPageFour != null) {
				ButtonAlertOKPageFour.Dispose ();
				ButtonAlertOKPageFour = null;
			}

			if (SliderPageFour != null) {
				SliderPageFour.Dispose ();
				SliderPageFour = null;
			}

			if (SwitchPageFour != null) {
				SwitchPageFour.Dispose ();
				SwitchPageFour = null;
			}

			if (SegmentedControlPageFour != null) {
				SegmentedControlPageFour.Dispose ();
				SegmentedControlPageFour = null;
			}
		}
	}
}
