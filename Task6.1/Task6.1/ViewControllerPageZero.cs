﻿using System;
using UIKit;

namespace Task6
{
	public partial class ViewControllerPageZero : ViewControllerBase
	{
		public ViewControllerPageZero() : base("ViewControllerPageZero", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			Title = "Task 6.1";

			SetButtonsHandlers();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		private void SetButtonsHandlers()
		{
			ButtonOnePageZero.TouchUpInside += (sender, e) =>
			{
				NavigateTo<ViewControllerPageOne>();
			};
			ButtonTwoPageZero.TouchUpInside += (sender, e) =>
			{
				NavigateTo<ViewControllerPageTwo>();
			};
			ButtonThreePageZero.TouchUpInside += (sender, e) =>
			{
				NavigateTo<ViewControllerPageThree>();
			};
			ButtonFourPageZero.TouchUpInside += (sender, e) =>
			{
				NavigateTo<ViewControllerPageFour>();
			};
		}
	}
}