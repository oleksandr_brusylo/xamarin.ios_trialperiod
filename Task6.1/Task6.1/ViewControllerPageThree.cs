﻿using System;

using UIKit;

namespace Task6
{
	public partial class ViewControllerPageThree : ViewControllerBase
	{
		public ViewControllerPageThree() : base("ViewControllerPageThree", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			Title = "Page Three";

			ImageViewPageThree.Image = UIImage.FromBundle("iOS_MVC.png");
			//ImageViewPageThree.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
			//ImageViewPageThree.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}