﻿using System;
using UIKit;

namespace Task6
{
	public partial class ViewControllerPageTwo : ViewControllerBase 
	{
		public ViewControllerPageTwo() : base("ViewControllerPageTwo", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			Title = "Page Two";
			InitializeUIControl();
			SetButtonHandler();
		}

		private void InitializeUIControl()
		{
			ClickMeButtonPageTwo.SetImage(UIImage.FromBundle("Icons/IconAppleGray.jpeg"), UIControlState.Normal);
			ClickMeButtonPageTwo.SetImage(UIImage.FromBundle("Icons/IconAppleBlack.jpeg"), UIControlState.Highlighted);
		}

		private void SetButtonHandler()
		{
			ClickMeButtonPageTwo.TouchUpInside += (sender, e) =>
			{
				var alertController = UIAlertController.Create("Apple", "iPhone is Awesome !", UIAlertControllerStyle.Alert);
				alertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
				PresentViewController(alertController, true, null);
			};
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}