﻿using System;
using UIKit;

namespace Task6
{
	public partial class ViewControllerPageFour : ViewControllerBase 
	{
		public ViewControllerPageFour() : base("ViewControllerPageFour", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			Title = "Page Four";
			InitializeUIControls();
		}

		private void InitializeUIControls() 
		{
			ExamineSliderProperties();
			ExamineSwitchProperties();
			ExamineSegmentedControlProperties();
			SetButtonsHandlers();
		}

		private void ExamineSliderProperties()
		{
			SliderPageFour.MinValue = 0f;
			SliderPageFour.MaxValue = 100f;
			SliderPageFour.Value = 50f;
			SliderPageFour.MinimumTrackTintColor = UIColor.Green;
			SliderPageFour.MaximumTrackTintColor = UIColor.Red;
			SliderPageFour.ValueChanged += (sender, e) =>
			{
				Console.WriteLine(SliderPageFour.Value);
				if (SliderPageFour.Value <= 25)
				{
					SwitchPageFour.Enabled = false;
				}
				else 
				{
					SwitchPageFour.Enabled = true;
				}
			};
		}

		private void ExamineSwitchProperties() 
		{
			SwitchPageFour.TouchUpInside += (sender, e) =>
			{
				SliderPageFour.Enabled = !SliderPageFour.Enabled;
			};
		}

		private void ExamineSegmentedControlProperties()
		{
			SegmentedControlPageFour.InsertSegment("Cyan", 2, true);
			SegmentedControlPageFour.InsertSegment("Magenta", 3, true);
			SegmentedControlPageFour.InsertSegment("Yellow", 4, true);
			SegmentedControlPageFour.ApportionsSegmentWidthsByContent = true;
			//SegmentedControlPageFour.SelectedSegment = 0;
			SegmentedControlPageFour.ValueChanged += (sender, e) =>
			{
				var segmentId = ((UISegmentedControl)sender).SelectedSegment;
				switch (SegmentedControlPageFour.SelectedSegment) 
				{
					case 0:
						View.BackgroundColor = UIColor.White;
					break;
					case 1:
						View.BackgroundColor = UIColor.Black;
						break;
					case 2:
						View.BackgroundColor = UIColor.Cyan;
						break;
					case 3:
						View.BackgroundColor = UIColor.Magenta;
						break;
					case 4:
						View.BackgroundColor = UIColor.Yellow;
						break;
					default:
						break;
						
				}
			};
		}

		private void SetButtonsHandlers()
		{
			RaiseAlertOK();
			RaiseAlertOKCancel();
			RaiseAlertOKCancelCustom();
		}

		private void RaiseAlertOK()
		{
			ButtonAlertOKPageFour.TouchUpInside += (sender, e) =>
			{
				var okAlertController = UIAlertController.Create("Title", "The message", UIAlertControllerStyle.Alert);
				okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
				PresentViewController(okAlertController, true, null);
			};
		}

		private void RaiseAlertOKCancel()
		{
			ButtonAlertOKCancelPageFour.TouchUpInside += ((sender, e) =>
			{
				var okCancelAlertController = UIAlertController.Create("Alert Title", "Choose from two buttons", UIAlertControllerStyle.Alert);
				okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
				okCancelAlertController.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Cancel was clicked")));
				PresentViewController(okCancelAlertController, true, null);
			});
		}

		void RaiseAlertOKCancelCustom()
		{
			ButtonAlertOKCustomCancelPageFour.TouchUpInside += ((sender, e) =>
			{
				//var okCancelAlertController = UIAlertController.Create("Alert Title", "Choose from two buttons", UIAlertControllerStyle.Alert);

				//okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
				//okCancelAlertController.AddAction(UIAlertAction.Create("Custom", UIAlertActionStyle.Destructive, alert => Console.WriteLine("Custom was clicked")));
				//okCancelAlertController.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, alert => Console.WriteLine("Cancel was clicked")));
				//PresentViewController(okCancelAlertController, true, null);

				UIAlertController actionSheetAlert = UIAlertController.Create("Action Sheet", "Select an item from below", UIAlertControllerStyle.ActionSheet);

				actionSheetAlert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (action) => Console.WriteLine("Item One pressed.")));

				actionSheetAlert.AddAction(UIAlertAction.Create("Custom Button", UIAlertActionStyle.Default, (action) => Console.WriteLine("Item Two pressed.")));

				actionSheetAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Default, (action) => Console.WriteLine("Item Three pressed.")));

				actionSheetAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, (action) => Console.WriteLine("Cancel button pressed.")));

				//Required for iPad - You must specify a source for the Action Sheet since it is
				//displayed as a popover

				UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
				if (presentationPopover != null)
				{
					presentationPopover.SourceView = this.View;
					presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
				}

				this.PresentViewController(actionSheetAlert, true, null);
			});
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}