﻿using UIKit;
using Foundation;

namespace Task6
{
	public class ViewControllerBase: UIViewController 
	{
		public ViewControllerBase() : base("ViewControllerBase", null)
		{
		}

		public ViewControllerBase(string nibName, NSBundle bundle) : base (nibName, bundle)
		{
		}

		public T NavigateTo<T>() where T : UIViewController, new() 
		{
			var vc = new T();
			this.NavigationController.PushViewController(vc, true);
			return vc;
		}
	}
}