// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6
{
	[Register ("ViewControllerPageZero")]
	partial class ViewControllerPageZero
	{
		[Outlet]
		UIKit.UIButton ButtonFourPageZero { get; set; }

		[Outlet]
		UIKit.UIButton ButtonOnePageZero { get; set; }

		[Outlet]
		UIKit.UIButton ButtonThreePageZero { get; set; }

		[Outlet]
		UIKit.UIButton ButtonTwoPageZero { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ButtonOnePageZero != null) {
				ButtonOnePageZero.Dispose ();
				ButtonOnePageZero = null;
			}

			if (ButtonTwoPageZero != null) {
				ButtonTwoPageZero.Dispose ();
				ButtonTwoPageZero = null;
			}

			if (ButtonThreePageZero != null) {
				ButtonThreePageZero.Dispose ();
				ButtonThreePageZero = null;
			}

			if (ButtonFourPageZero != null) {
				ButtonFourPageZero.Dispose ();
				ButtonFourPageZero = null;
			}
		}
	}
}
