// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6
{
	[Register ("ViewControllerPageOne")]
	partial class ViewControllerPageOne
	{
		[Outlet]
		UIKit.UITextField TextFieldPageOne { get; set; }

		[Outlet]
		UIKit.UITextView TextViewPageOne { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (TextFieldPageOne != null) {
				TextFieldPageOne.Dispose ();
				TextFieldPageOne = null;
			}

			if (TextViewPageOne != null) {
				TextViewPageOne.Dispose ();
				TextViewPageOne = null;
			}
		}
	}
}
