﻿using System;

using UIKit;

namespace Task6
{
	public partial class ViewControllerPageOne : ViewControllerBase
	{
		public ViewControllerPageOne() : base("ViewControllerPageOne", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			Title = "Page One";
			InitializeUIControls();
			BindTextFieldToTextView();
		}

		private void InitializeUIControls()
		{
			ExamineTextFieldProperties();
			ExamineTextViewProperties();
			
		}

		private void ExamineTextFieldProperties() 
		{
			TextFieldPageOne.Placeholder = "Hello, Xamarin.iOS";
			TextFieldPageOne.ClearButtonMode = UITextFieldViewMode.WhileEditing;

			//Seems not working on simulator...
			//TextFieldPageOne.MinimumFontSize = 17f;
			//TextFieldPageOne.AdjustsFontSizeToFitWidth = true;

			TextFieldPageOne.AutocapitalizationType = UITextAutocapitalizationType.None;
			TextFieldPageOne.AutocorrectionType = UITextAutocorrectionType.Yes;

			//Seems not working on simulator...
			//TextFieldPageOne.KeyboardAppearance = UIKeyboardAppearance.Alert;
			//TextFieldPageOne.KeyboardType = UIKeyboardType.NumberPad;
			//TextFieldPageOne.ReturnKeyType = UIReturnKeyType.EmergencyCall;

			TextFieldPageOne.SecureTextEntry = true;
		}

		private void ExamineTextViewProperties() 
		{
			TextViewPageOne.Text = "https://developer.xamarin.com/guides/ios/user_interface/controls";
			TextViewPageOne.Editable = false; // if 'true' DataDetector is not working !
			TextViewPageOne.DataDetectorTypes = UIDataDetectorType.All;
		}

		private void BindTextFieldToTextView() 
		{
			TextFieldPageOne.EditingChanged += (sender, e) => 
			{
				TextViewPageOne.Text = ((UITextField)sender).Text;
			};
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}