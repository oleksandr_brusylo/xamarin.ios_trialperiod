﻿using UIKit;
namespace Task6._1
{
	public class TableItem
	{
		public TableItem() {}

		public TableItem(string heading)
		{
			Heading = heading;
		}

		protected UITableViewCellStyle cellStyle = UITableViewCellStyle.Default;

		protected UITableViewCellAccessory cellAccessory = UITableViewCellAccessory.DetailButton;

		public string Heading { get; set; }

		public string SubHeading { get; set; }

		public string ImageName { get; set; }

		public UITableViewCellStyle CellStyle
		{
			get { return cellStyle; }
			set { cellStyle = value; }
		}

		public UITableViewCellAccessory CellAccessory
		{
			get { return cellAccessory; }
			set { cellAccessory = value; }
		}
	}
}