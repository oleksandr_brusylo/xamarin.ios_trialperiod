﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace Task6._1
{
public class TableSource : UITableViewSource
	{
		List<TableItem> tableItems;
		CellType enumCellIdentifier;
		Dictionary<CellType, UITableViewCellStyle> enumToCellStyle;
		MainViewController owner;

		public TableSource(List<TableItem> items, MainViewController owner)
		{
			tableItems = items;
			this.owner = owner;

			enumToCellStyle = new Dictionary<CellType, UITableViewCellStyle>()
			{
				{CellType.Default,UITableViewCellStyle.Default},
				{CellType.Subtitle, UITableViewCellStyle.Subtitle},
				{CellType.Value1, UITableViewCellStyle.Value1},
				{CellType.Value2, UITableViewCellStyle.Value2}
			};
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return tableItems.Count;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			UIAlertController okAlertController = UIAlertController.Create("Row Selected", 
			                                                               tableItems[indexPath.Row].Heading + "\n" +
			                                                               tableItems[indexPath.Row].SubHeading,
			                                                               UIAlertControllerStyle.Alert);
			okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
			owner.PresentViewController(okAlertController, true, null);
			tableView.DeselectRow(indexPath, true);
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			////Custom cell style
			//var cell = tableView.DequeueReusableCell(enumToCellStyle.ToString()) as CustomVegeCell;
			//if (cell == null)
			//	cell = new CustomVegeCell((NSString)enumToCellStyle.ToString());
			//cell.UpdateCell(tableItems[indexPath.Row].Heading
			//		, tableItems[indexPath.Row].SubHeading
			//		, UIImage.FromFile("Images/" + tableItems[indexPath.Row].ImageName));
			//return cell;
			//// --------------------------------------------------------------------------------

			SetCellType(indexPath);

			// request a recycled cell to save memory
			UITableViewCell cell = tableView.DequeueReusableCell(enumCellIdentifier.ToString());

			var cellStyle = enumToCellStyle[enumCellIdentifier];

			if (cell == null)
			{
				cell = new UITableViewCell(cellStyle, enumCellIdentifier.ToString());
			}

			cell.TextLabel.Text = tableItems[indexPath.Row].Heading;

			// Default style doesn't support Subtitle
			if (cellStyle != UITableViewCellStyle.Default)
			{
				cell.DetailTextLabel.Text = tableItems[indexPath.Row].SubHeading;
			}

			// Value2 style doesn't support an image
			if (cellStyle != UITableViewCellStyle.Value2)
				cell.ImageView.Image = UIImage.FromFile("Images/" + tableItems[indexPath.Row].ImageName);

			return cell;
		}

		public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, 
		                                        Foundation.NSIndexPath indexPath)
		{
			switch (editingStyle)
			{
				case UITableViewCellEditingStyle.Delete:
					// remove the item from the underlying data source
					tableItems.RemoveAt(indexPath.Row);
					// delete the row from the table
					tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
					break;
				case UITableViewCellEditingStyle.None:
					Console.WriteLine("CommitEditingStyle:None called");
					break;
			}
		}

		public override string TitleForDeleteConfirmation(UITableView tableView, NSIndexPath indexPath)
		{   // Optional - default text is 'Delete'
			return "Delete (" + tableItems[indexPath.Row].Heading + ") ?";
		}

		private void SetCellType(NSIndexPath path) 
		{
			switch (path.Row)
			{
				case 0:
					enumCellIdentifier = CellType.Default;
					break;
				case 1:
					enumCellIdentifier = CellType.Subtitle;
					break;
				case 2:
					enumCellIdentifier = CellType.Value1;
					break;
				case 3:
					enumCellIdentifier = CellType.Value2;
					break;
				default:
					enumCellIdentifier = CellType.Default;
					break;
			}
		}
	}
}