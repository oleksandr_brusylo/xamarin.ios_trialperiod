﻿using UIKit;
using System.Collections.Generic;
using CoreGraphics;

namespace Task6._1
{
	public partial class MainViewController : UIViewController
	{
		public MainViewController() : base("MainViewController", null)
		{}

		UITableView table;

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			table = new UITableView(new CGRect(0, 20, View.Bounds.Width, View.Bounds.Height - 20));
			table.AutoresizingMask = UIViewAutoresizing.All;

			List<TableItem> tableItems = new List<TableItem>();

			tableItems.Add(new TableItem("Vegetables") { SubHeading = "65 items", ImageName = "Vegetables.jpg" });
			tableItems.Add(new TableItem("Fruits") { SubHeading = "17 items", ImageName = "Fruits.jpg" });
			tableItems.Add(new TableItem("Flower Buds") { SubHeading = "5 items", ImageName = "Flower Buds.jpg" });
			tableItems.Add(new TableItem("Legumes") { SubHeading = "33 items", ImageName = "Legumes.jpg" });
			tableItems.Add(new TableItem("Bulbs") { SubHeading = "18 items", ImageName = "Bulbs.jpg" });
			tableItems.Add(new TableItem("Tubers") { SubHeading = "43 items", ImageName = "Tubers.jpg" });
			table.Source = new TableSource(tableItems, this);
			Add(table);
		}
	}
}