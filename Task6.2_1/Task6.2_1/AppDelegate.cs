﻿using Foundation;
using UIKit;

namespace Task6._1
{
	[Register("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate
	{

		public override UIWindow Window
		{
			get;
			set;
		}

		protected MainViewController mainVC;

		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
			Window.MakeKeyAndVisible();
			mainVC = new MainViewController();
			mainVC.Title = "Task6.2_1";
			Window.RootViewController = mainVC;
			return true;
		}

		public override void OnResignActivation(UIApplication application)
		{}

		public override void DidEnterBackground(UIApplication application)
		{}

		public override void WillEnterForeground(UIApplication application)
		{}

		public override void OnActivated(UIApplication application)
		{}

		public override void WillTerminate(UIApplication application)
		{}
	}
}