﻿using UIKit;

namespace Task6
{
	public partial class FifthViewController : BaseViewController
	{
		public FifthViewController() : base("FifthViewController", null)
		{
			title = "Blue";
			mainColor = UIColor.FromRGB(178, 234, 255);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			SetAppearanceOfController(title, mainColor);
		}
	}
}