﻿using UIKit;

namespace Task6
{
	public partial class FourthViewController : BaseViewController
	{
		public FourthViewController() : base("FourthViewController", null)
		{
			title = "Red";
			mainColor = UIColor.FromRGB(255, 84, 95);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			SetAppearanceOfController(title, mainColor);
			SetRightBarButton<FifthViewController>();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			NavigationController.NavigationBar.BarTintColor = mainColor;
		}
	}
}