﻿using UIKit;

namespace Task6
{
	public partial class ThirdViewController : BaseViewController
	{
		public ThirdViewController() : base("ThirdViewController", null)
		{
			title = "Gray";
			mainColor = UIColor.FromRGB(110, 140, 166);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			SetAppearanceOfController(title, mainColor);
			SetRightBarButton<FourthViewController>();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			NavigationController.NavigationBar.BarTintColor = mainColor;
		}
	}
}