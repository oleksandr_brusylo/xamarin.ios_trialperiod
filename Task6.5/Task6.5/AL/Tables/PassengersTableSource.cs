﻿using UIKit;
using System;
using Foundation;
using System.Collections.Generic;

namespace Task6
{
	public class PassengersTableSource : UITableViewSource
	{
		public List<Passenger> passengers;
		
		public PassengersTableSource(List<Passenger> passengers)
		{
			this.passengers = passengers;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var passenger = passengers[indexPath.Row];
			var cell = (PassengersCell)tableView.DequeueReusableCell(PassengersCell.Key);

			if (cell == null)
			{
				cell = PassengersCell.Create();
			}

			cell.Update(passenger);

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return passengers.Count;
		}
	}
}