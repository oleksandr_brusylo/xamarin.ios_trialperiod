﻿using UIKit;
using System;
using Foundation;
using CoreGraphics;

namespace Task6
{
	public class DetailsTableSource : UITableViewSource
	{
		const int BigMargin = 20;
		const int SmallMargin = 10;
		const int TopViewHeight = 140;
		const int InnerTableExpansionOffset = 156;

		public Trip trip;

		public DetailsTableSource(Trip trip)
		{
			this.trip = trip;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var leg = trip.Legs[indexPath.Row];
			var cell = (DetailsCustomCell)tableView.DequeueReusableCell(DetailsCustomCell.Key);

			if (cell == null)
			{
				cell = DetailsCustomCell.Create();
			}

			cell.Update(leg);

			var innerTableHeight = cell.GetTransfersTableHeight();

			AddLeftSeparator(cell);
			AddRightSeparator(cell);
			AddBottomSeparator(cell, innerTableHeight);
			AddTransparentView(cell, innerTableHeight);

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return trip.Legs.Count;
		}

		private void AddBottomSeparator(UITableViewCell cell, NSLayoutConstraint innerTableHeight)
		{
			var bottomSeparator = new UIView(new CGRect(0, InnerTableExpansionOffset + innerTableHeight.Constant,
			                                            cell.Frame.Width, SmallMargin));
			bottomSeparator.BackgroundColor = UIColor.FromRGB(228, 228, 228);
			cell.AddSubview(bottomSeparator);
			cell.SendSubviewToBack(bottomSeparator);
		}

		private void AddLeftSeparator(UITableViewCell cell)
		{
			var leftSeparator = new UIView(new CGRect(0, 0, SmallMargin, cell.Frame.Height - SmallMargin));
			leftSeparator.BackgroundColor = UIColor.FromRGB(228, 228, 228);
			cell.AddSubview(leftSeparator);
			cell.SendSubviewToBack(leftSeparator);
		}

		private void AddRightSeparator(UITableViewCell cell)
		{
			var rightSeparator = new UIView(new CGRect(cell.Frame.Width - SmallMargin, 0, SmallMargin,
			                                           cell.Frame.Height - SmallMargin));
			rightSeparator.BackgroundColor = UIColor.FromRGB(228, 228, 228);
			cell.AddSubview(rightSeparator);
			cell.SendSubviewToBack(rightSeparator);
		}

		private void AddTransparentView(UITableViewCell cell, NSLayoutConstraint innerTableHeight)
		{
			var transparentView = new UIView(new CGRect(SmallMargin, 0, cell.Frame.Width - BigMargin, 
			                                            TopViewHeight + BigMargin + innerTableHeight.Constant));
			transparentView.Layer.BorderColor = UIColor.FromRGB(208, 208, 208).CGColor;
			transparentView.Layer.BorderWidth = 1.0f;

			cell.AddSubview(transparentView);
			cell.SendSubviewToBack(transparentView);
		}
	}
}