﻿using UIKit;
using System;
using Foundation;
using System.Collections.Generic;

namespace Task6
{
	public class WeatherTableSource : UITableViewSource
	{
		List<Weather> weatherForecasts;

		public WeatherTableSource(List<Weather> items)
		{
			weatherForecasts = items;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var weatherForecast = weatherForecasts[indexPath.Row];
			var cell = (WeatherCustomCell)tableView.DequeueReusableCell(WeatherCustomCell.Key);

			if (cell == null)
			{
				cell = WeatherCustomCell.Create();
			}

			cell.Update(weatherForecast);

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			cell.LayoutMargins = UIEdgeInsets.Zero;
			cell.SeparatorInset = UIEdgeInsets.Zero;

			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return weatherForecasts.Count;
		}
	}
}