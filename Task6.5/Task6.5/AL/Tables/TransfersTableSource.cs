﻿using UIKit;
using System;
using Foundation;
using System.Collections.Generic;

namespace Task6
{
	public class TransfersTableSource : UITableViewSource
	{
		public List<Transfer> transfers;

		public event EventHandler<SelectedRowEventArgs> UserSelectsRow;

		UIViewController detailsVC;
		TransfersViewController transfersVC;

		public TransfersTableSource(UIViewController detailsVC, TransfersViewController transfersVC)
		{
			this.detailsVC = detailsVC;
			this.transfersVC = transfersVC;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var transfer = transfers[indexPath.Row];
			var cell = (TransferCustomCell)tableView.DequeueReusableCell(TransferCustomCell.Key);

			if (cell == null)
			{
				cell = TransferCustomCell.Create();
			}

			cell.Update(transfer);

			cell.LayoutMargins = UIEdgeInsets.Zero;
			cell.SeparatorInset = UIEdgeInsets.Zero;
			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return transfers.Count;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return TransferCustomCell.CellHeight;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			detailsVC.NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);

			//foreach (var tmpVC in detailsVC.NavigationController.ViewControllers)		//TODO: unknown bug with tabs.. 
			//{
			//	if (tmpVC.Equals(transfersVC)) 
			//	{
			//		transfersVC.RemoveFromParentViewController();
			//	}
			//}

			detailsVC.NavigationController.PushViewController(transfersVC, true);	
			
			if (indexPath.Row < transfers.Count - 1)
				transfersVC.NextTransfer = transfers[indexPath.Row + 1];

			transfersVC.CurrentTransfer = transfers[indexPath.Row];
			OnUserSelectedRow(new SelectedRowEventArgs(indexPath.Row));
			tableView.DeselectRow(indexPath, true);
		}

		protected virtual void OnUserSelectedRow(SelectedRowEventArgs e)
		{
			EventHandler<SelectedRowEventArgs> handler = UserSelectsRow;

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public Transfer GetItem(int id)
		{
			return transfers[id];
		}
	}
}