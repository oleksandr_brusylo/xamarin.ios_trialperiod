﻿namespace Task6
{
	public class SelectedRowEventArgs
	{
		private int selectedRowId;

		public int RowId
		{
			get { return selectedRowId; }
			set { selectedRowId = value; }
		}

		public SelectedRowEventArgs(int selectedRowId)
		{
			this.selectedRowId = selectedRowId;
		}
	}
}