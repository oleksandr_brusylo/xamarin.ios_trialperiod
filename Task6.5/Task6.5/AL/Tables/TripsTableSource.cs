﻿using UIKit;
using System;
using Foundation;
using CoreGraphics;
using System.Collections.Generic;

namespace Task6
{
	public class TripsTableSource : UITableViewSource
	{
		List<Trip> repository;
		UIViewController detailsVC;
		UIViewController upcomingVC;

		const int SmallMargin = 8;

		public event EventHandler<SelectedRowEventArgs> UserSelectsRow;

		public TripsTableSource(UIViewController upcomingVC, UIViewController detailsVC, List<Trip> repository)
		{
			this.repository = repository;
			this.upcomingVC = upcomingVC;
			this.detailsVC = detailsVC;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var trip = repository[indexPath.Row];
			
			var cell = (CustomCell)tableView.DequeueReusableCell(CustomCell.Key);

			if (cell == null)
			{
				cell = CustomCell.Create();
			}

			cell.Update(trip);

			AddBottomSeparator(cell);
			AddTransparentView(cell);

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;
		}

		private void AddBottomSeparator(UITableViewCell cell)
		{
			var bottomSeparator = new UIView(new CGRect(0, cell.Frame.Height - SmallMargin, cell.Frame.Width, 
			                                            SmallMargin));
			bottomSeparator.BackgroundColor = UIColor.FromRGB(228, 228, 228);
			cell.AddSubview(bottomSeparator);
		}

		private void AddTransparentView(UITableViewCell cell)
		{
			var transparentView = new UIView(new CGRect(0, 0, cell.Frame.Width, cell.Frame.Height - SmallMargin));
			transparentView.Layer.BorderColor = UIColor.FromRGB(208, 208, 208).CGColor;
			transparentView.Layer.BorderWidth = 1.0f;
			cell.AddSubview(transparentView);
			cell.SendSubviewToBack(transparentView);
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return repository.Count;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return CustomCell.CellHeight;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			upcomingVC.NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
			upcomingVC.NavigationController.PushViewController(detailsVC, true);
			OnUserSelectedRow(new SelectedRowEventArgs(indexPath.Row));
		}

		protected virtual void OnUserSelectedRow(SelectedRowEventArgs e)
		{
			EventHandler<SelectedRowEventArgs> handler = UserSelectsRow;

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public Trip GetItem(int id)
		{
			return repository[id];
		}
	}
}