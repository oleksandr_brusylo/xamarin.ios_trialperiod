﻿using UIKit;
using System;
using Foundation;
using System.Collections.Generic;

namespace Task6
{
	public partial class TransferCustomCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("TransferCustomCell");
		public static readonly UINib Nib;
		public static nfloat CellHeight = 125;

		static Dictionary<string, UIColor> statusColor = new Dictionary<string, UIColor>() 
		{
			{"Landed", UIColor.FromRGB(0, 200, 0)},
			{"Check-In", UIColor.FromRGB(255, 165, 0)},
			{"Go To Gate", UIColor.FromRGB(255, 0, 0)},
			{"In Progress", UIColor.FromRGB(210,180,140)},
			{"", UIColor.White}
		}; 

		static TransferCustomCell()
		{
			Nib = UINib.FromName("TransferCustomCell", NSBundle.MainBundle);
		}

		protected TransferCustomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public static TransferCustomCell Create()
		{
			return (TransferCustomCell)Nib.Instantiate(null, null)[0];
		}

		Transfer Transfer
		{
			get;
			set;
		}

		public void Update(Transfer transfer)
		{
			Transfer = transfer;
			routeLabel.Text = Transfer.GetRouteFull;
			transferNumberLabel.Text = Transfer.TransferNumber;

			statusLabel.Text = Transfer.Status;
			statusLabel.TextColor = statusColor[Transfer.Status];

			flightDurationLabel.Text = 
				string.Format("{0}h{1}m", Transfer.Duration.ToString("%H"), Transfer.Duration.ToString("%m"));

			SetLayover();
		}

		private void SetLayover()
		{
			if (Transfer.Layover.Equals(default(DateTime)))
			{
				layoverLabel.Text = "-";
			}
			else
			{
				layoverLabel.Text =
					string.Format("{0}h{1}m", Transfer.Layover.ToString("%H"), Transfer.Layover.ToString("%m"));
			}
		}
	}
}