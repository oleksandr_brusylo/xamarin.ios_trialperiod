// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6
{
	[Register ("WeatherCustomCell")]
	partial class WeatherCustomCell
	{
		[Outlet]
		UIKit.UILabel ForecastCondition { get; set; }

		[Outlet]
		UIKit.UILabel ForecastDate { get; set; }

		[Outlet]
		UIKit.UIImageView ForecastImage { get; set; }

		[Outlet]
		UIKit.UILabel ForecastTemperature { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ForecastDate != null) {
				ForecastDate.Dispose ();
				ForecastDate = null;
			}

			if (ForecastImage != null) {
				ForecastImage.Dispose ();
				ForecastImage = null;
			}

			if (ForecastTemperature != null) {
				ForecastTemperature.Dispose ();
				ForecastTemperature = null;
			}

			if (ForecastCondition != null) {
				ForecastCondition.Dispose ();
				ForecastCondition = null;
			}
		}
	}
}
