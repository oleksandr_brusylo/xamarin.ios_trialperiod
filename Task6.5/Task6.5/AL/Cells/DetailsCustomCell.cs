﻿using UIKit;
using System;
using Foundation;

namespace Task6
{
	public partial class DetailsCustomCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("DetailsCustomCell");
		public static readonly UINib Nib;

		string formattingMask = "ddd, MMM dd, hh:mm tt";

		static UIViewController ownerVC;

		static DetailsCustomCell()
		{
			Nib = UINib.FromName("DetailsCustomCell", NSBundle.MainBundle);
		}

		protected DetailsCustomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public static DetailsCustomCell Create()
		{
			return (DetailsCustomCell)Nib.Instantiate(null, null)[0];
		}

		public static void SetOwnerVC(UIViewController owner)
		{
			ownerVC = owner;
		}

		Leg Leg
		{
			get;
			set;
		}

		public NSLayoutConstraint GetTransfersTableHeight()
		{
			return innerTableHeight;
		}

		public void Update(Leg leg)
		{
			Leg = leg;
			
			UpdateInnerTable();

			UpdateDepartureViews();

			UpdateArrivalViews();

			((TransfersTableSource)tableViewTransferPoints.Source).transfers = Leg.Transfers;
			innerTableHeight.Constant = TransferCustomCell.CellHeight * Leg.Transfers.Count;
		}

		private void UpdateInnerTable()
		{
			var transfersVC = new TransfersViewController();
			var source = new TransfersTableSource(ownerVC, transfersVC);

			transfersVC.TableSource = source;
			transfersVC.LastTransfer = Leg.GetLastTransfer;
			tableViewTransferPoints.Source = source;
		}

		private void UpdateDepartureViews() 
		{
			departureAcronym.Text = Leg.DeparturePoint.Acronym;
			departureName.Text = Leg.DeparturePoint.FullName;
			departureTime.Text = Leg.DepartureTime.ToString(formattingMask);
		}

		private void UpdateArrivalViews()
		{
			arrivalAcronym.Text = Leg.ArrivalPoint.Acronym;
			arrivalName.Text = Leg.ArrivalPoint.FullName;
			arrivalTime.Text = Leg.ArrivalTime.ToString(formattingMask);
			travelDuration.Text = OneDayDurationFormatting();
		}

		private string OneDayDurationFormatting()
		{
			var duration = string.Format("{0}h{1}m", Leg.TravelTime.ToString("%H"), Leg.TravelTime.ToString("%m"));
			if (string.Equals(duration, "0h0m")) 
			{
				duration = "24h0m";
			}
			return duration;
		}
	}
}