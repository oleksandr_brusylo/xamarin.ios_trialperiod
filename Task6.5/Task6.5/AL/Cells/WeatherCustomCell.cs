﻿using UIKit;
using System;
using Foundation;

namespace Task6
{
	public partial class WeatherCustomCell : UITableViewCell
	{
		public static nfloat CellHeight = 170;
		public static readonly NSString Key = new NSString("WeatherCustomCell");
		public static readonly UINib Nib;
		string formattingMask = "MMM dd";

		static WeatherCustomCell()
		{
			Nib = UINib.FromName("WeatherCustomCell", NSBundle.MainBundle);
		}

		protected WeatherCustomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public static WeatherCustomCell Create()
		{
			return (WeatherCustomCell)Nib.Instantiate(null, null)[0];
		}

		public void Update(Weather weather)
		{
			ForecastDate.Text = weather.ForecastDate.ToString(formattingMask);
			ForecastCondition.Text = weather.ForecastCondition;
			ForecastImage.Image = UIImage.FromBundle(WeatherManager.GetWeatherIconPath(weather.ForecastCondition));
			ForecastTemperature.Text = string.Format("{0}{1}", weather.ForecastTemperature, "C");
		}
	}
}