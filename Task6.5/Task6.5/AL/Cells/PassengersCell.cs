﻿using UIKit;
using System;
using Foundation;

namespace Task6
{
	public partial class PassengersCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("PassengersCell");
		public static readonly UINib Nib;
		public static nfloat CellHeight = 25;

		static PassengersCell()
		{
			Nib = UINib.FromName("PassengersCell", NSBundle.MainBundle);
		}

		protected PassengersCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public static PassengersCell Create()
		{
			return (PassengersCell)Nib.Instantiate(null, null)[0];
		}

		public void Update(Passenger passenger)
		{
			FullPassengerName.Text = passenger.GetFullName;
			PassengerNumber.Text = passenger.PassengerNumber;
		}
	}
}