// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Task6
{
    [Register ("PassengersCell")]
    partial class PassengersCell
    {
        [Outlet]
        UIKit.UILabel FullPassengerName { get; set; }


        [Outlet]
        UIKit.UILabel PassengerNumber { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (FullPassengerName != null) {
                FullPassengerName.Dispose ();
                FullPassengerName = null;
            }

            if (PassengerNumber != null) {
                PassengerNumber.Dispose ();
                PassengerNumber = null;
            }
        }
    }
}