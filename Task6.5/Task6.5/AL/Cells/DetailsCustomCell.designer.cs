// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Task6
{
    [Register ("DetailsCustomCell")]
    partial class DetailsCustomCell
    {
        [Outlet]
        UIKit.UILabel arrivalAcronym { get; set; }


        [Outlet]
        UIKit.UILabel arrivalName { get; set; }


        [Outlet]
        UIKit.UILabel arrivalTime { get; set; }


        [Outlet]
        UIKit.UILabel departureAcronym { get; set; }


        [Outlet]
        UIKit.UILabel departureName { get; set; }


        [Outlet]
        UIKit.UILabel departureTime { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint innerTableHeight { get; set; }


        [Outlet]
        UIKit.UITableView tableViewTransferPoints { get; set; }


        [Outlet]
        UIKit.UILabel travelDuration { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (arrivalAcronym != null) {
                arrivalAcronym.Dispose ();
                arrivalAcronym = null;
            }

            if (arrivalName != null) {
                arrivalName.Dispose ();
                arrivalName = null;
            }

            if (arrivalTime != null) {
                arrivalTime.Dispose ();
                arrivalTime = null;
            }

            if (departureAcronym != null) {
                departureAcronym.Dispose ();
                departureAcronym = null;
            }

            if (departureName != null) {
                departureName.Dispose ();
                departureName = null;
            }

            if (departureTime != null) {
                departureTime.Dispose ();
                departureTime = null;
            }

            if (innerTableHeight != null) {
                innerTableHeight.Dispose ();
                innerTableHeight = null;
            }

            if (tableViewTransferPoints != null) {
                tableViewTransferPoints.Dispose ();
                tableViewTransferPoints = null;
            }

            if (travelDuration != null) {
                travelDuration.Dispose ();
                travelDuration = null;
            }
        }
    }
}