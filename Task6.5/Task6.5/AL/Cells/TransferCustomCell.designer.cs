// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Task6
{
    [Register ("TransferCustomCell")]
    partial class TransferCustomCell
    {
        [Outlet]
        UIKit.UILabel flightDurationLabel { get; set; }


        [Outlet]
        UIKit.UILabel flightDurationPlaceholder { get; set; }


        [Outlet]
        UIKit.UILabel layouverPlaceholder { get; set; }


        [Outlet]
        UIKit.UILabel layoverLabel { get; set; }


        [Outlet]
        UIKit.UILabel routeLabel { get; set; }


        [Outlet]
        UIKit.UILabel statusLabel { get; set; }


        [Outlet]
        UIKit.UILabel transferNumberLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (flightDurationLabel != null) {
                flightDurationLabel.Dispose ();
                flightDurationLabel = null;
            }

            if (flightDurationPlaceholder != null) {
                flightDurationPlaceholder.Dispose ();
                flightDurationPlaceholder = null;
            }

            if (layouverPlaceholder != null) {
                layouverPlaceholder.Dispose ();
                layouverPlaceholder = null;
            }

            if (layoverLabel != null) {
                layoverLabel.Dispose ();
                layoverLabel = null;
            }

            if (routeLabel != null) {
                routeLabel.Dispose ();
                routeLabel = null;
            }

            if (statusLabel != null) {
                statusLabel.Dispose ();
                statusLabel = null;
            }

            if (transferNumberLabel != null) {
                transferNumberLabel.Dispose ();
                transferNumberLabel = null;
            }
        }
    }
}