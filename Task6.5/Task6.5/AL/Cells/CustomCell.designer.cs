// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Task6
{
    [Register ("CustomCell")]
    partial class CustomCell
    {
        [Outlet]
        UIKit.UILabel labelDepartureDate { get; set; }


        [Outlet]
        UIKit.UILabel labelDestination { get; set; }


        [Outlet]
        UIKit.UILabel labelReturnDate { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (labelDepartureDate != null) {
                labelDepartureDate.Dispose ();
                labelDepartureDate = null;
            }

            if (labelDestination != null) {
                labelDestination.Dispose ();
                labelDestination = null;
            }

            if (labelReturnDate != null) {
                labelReturnDate.Dispose ();
                labelReturnDate = null;
            }
        }
    }
}