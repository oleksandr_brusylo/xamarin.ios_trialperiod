﻿using UIKit;
using System;
using Foundation;

namespace Task6
{
	public partial class CustomCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("CustomCell");
		public static readonly UINib Nib;
		public static nfloat CellHeight = 105;
		string formattingMask = "MMM dd, hh:mm tt";

		static CustomCell()
		{
			Nib = UINib.FromName("CustomCell", NSBundle.MainBundle);
		}

		protected CustomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public static CustomCell Create()
		{
			return (CustomCell)Nib.Instantiate(null, null)[0];
		}

		public void Update(Trip trip)
		{
			labelDestination.Text = trip.Route.GetFullRoute;
			labelDepartureDate.Text = trip.DepartureDate.ToString(formattingMask);
			labelReturnDate.Text = trip.ReturnDate.ToString(formattingMask);
		}
	}
}