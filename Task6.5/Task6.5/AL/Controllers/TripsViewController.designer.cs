// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Task6
{
    [Register ("TripsViewController")]
    partial class TripsViewController
    {
        [Outlet]
        UIKit.UITableView UpcomingTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (UpcomingTableView != null) {
                UpcomingTableView.Dispose ();
                UpcomingTableView = null;
            }
        }
    }
}