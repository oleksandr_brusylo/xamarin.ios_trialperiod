﻿using UIKit;
using System;
using System.Linq;

namespace Task6
{
	public class TabController : UITabBarController
	{
		UIViewController upcomingTab, pastTab;

		UINavigationController navControllerTabOne;
		UINavigationController navControllerTabTwo;

		UIViewController upcomingVC;
		UIViewController pastVC;

		TripsRepository repository;

		public TabController()
		{
			repository = new TripsRepository();

			upcomingVC = new TripsViewController(repository.Trips.Where(p => p.ReturnDate > DateTime.Now).ToList());
			navControllerTabOne = new UINavigationController(upcomingVC);
			upcomingTab = navControllerTabOne;
			upcomingTab.Title = "Upcoming";

			pastVC = new TripsViewController(repository.Trips.Where(p => p.ReturnDate <= DateTime.Now).ToList());
			navControllerTabTwo = new UINavigationController(pastVC);
			pastTab = navControllerTabTwo;
			pastTab.Title = "Past";

			var tabs = new UIViewController[] { upcomingTab, pastTab };
			ViewControllers = tabs;
		}
	}
}