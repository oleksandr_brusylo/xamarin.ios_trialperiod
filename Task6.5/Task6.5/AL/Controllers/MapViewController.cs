﻿using UIKit;
using MapKit;
using System;
using CoreLocation;

namespace Task6
{
	public partial class MapViewController : UIViewController
	{
		Airport Airport { get; set; }
		IMapManager Manager {get; set; }
		double areaToDisplay = 20;

		public MapViewController(Airport airport, IMapManager manager)
		{
			Airport = airport;
			Manager = manager;
		}

		public MapViewController() : base("MapViewController", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = Airport.FullName;

			var mapView = new MKMapView(View.Bounds);
			mapView.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			View.Add(mapView);

			var latitude = Convert.ToDouble(Airport.Latitude);
			var longitude = Convert.ToDouble(Airport.Longitude);

			var coords = new CLLocationCoordinate2D(latitude, longitude);
			var span = new MKCoordinateSpan(Manager.DistanceToLatitudeDegrees(areaToDisplay), 
			                                Manager.DistanceToLongitudeDegrees(areaToDisplay, coords.Latitude));
			mapView.Region = new MKCoordinateRegion(coords, span);

			var annotation = new MapAnnotation(coords, Airport.Acronym, Airport.FullName);
			mapView.AddAnnotation(annotation);
		}
	}
}