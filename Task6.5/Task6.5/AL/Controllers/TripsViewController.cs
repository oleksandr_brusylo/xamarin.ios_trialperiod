﻿using UIKit;
using System.Collections.Generic;

namespace Task6
{
	public partial class TripsViewController : UIViewController
	{
		List<Trip> repository;
		TripsTableSource source;
		DetailsViewController detailsVC;

		public TripsViewController(List<Trip> repository)
		{
			this.repository = repository;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(0, 122, 255);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			detailsVC = new DetailsViewController();

			UpcomingTableView.RegisterNibForCellReuse(CustomCell.Nib, CustomCell.Key);
			source = new TripsTableSource(this, detailsVC, repository);
			UpcomingTableView.Source = source;
			source.UserSelectsRow += SetSelectedTrip;
		}

		private void SetSelectedTrip(object sender, SelectedRowEventArgs args)
		{
			detailsVC.SetTrip(source.GetItem(args.RowId));
		}
	}
}