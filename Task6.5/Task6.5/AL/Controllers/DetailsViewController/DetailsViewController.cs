﻿using UIKit;

namespace Task6
{
	public partial class DetailsViewController : UIViewController
	{
		Trip selectedTrip;
		TableHeader header;
		DetailsTableSource source;

		public DetailsViewController() : base("DetailsViewController", null)
		{}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Title = "TRIP DETAILS";

			NavigationController.NavigationBar.TintColor = UIColor.White;
			NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes()
			{
				ForegroundColor = UIColor.White
			};

			source = new DetailsTableSource(selectedTrip);
			DetailsTableView.RegisterNibForCellReuse(DetailsCustomCell.Nib, DetailsCustomCell.Key);
			DetailsTableView.Source = source;
			DetailsCustomCell.SetOwnerVC(this);
			header = TableHeader.Create(this);
			DetailsTableView.TableHeaderView = header;
			DetailsTableView.RowHeight = UITableView.AutomaticDimension;
			DetailsTableView.EstimatedRowHeight = 500;
		}

		public override void ViewDidLayoutSubviews() 
		{
			base.ViewDidLayoutSubviews();
			DetailsTableView.ReloadData();
			UpdateHeaderHeight();
		}

		private void UpdateHeaderHeight()
		{
			var frame = header.Frame;
			frame.Height = header.Height;
			header.Frame = frame;
		}

		public void SetTrip(Trip trip)
		{
			selectedTrip = trip;
			TableHeader.SetTrip(trip);
		}
	}
}