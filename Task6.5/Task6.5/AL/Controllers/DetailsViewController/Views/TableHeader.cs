using UIKit;
using System;
using Foundation;
using ObjCRuntime;

namespace Task6
{
    public partial class TableHeader : UIView
    {
		public nfloat Height { get; set; }

		static UIViewController ownerVC;
		static Trip selectedTrip;
		static nfloat ExtraSpace = 50;

		public static TableHeader Create(UIViewController viewController)
		{
			var array = NSBundle.MainBundle.LoadNib("TableHeader", null, null);
			var view = Runtime.GetNSObject<TableHeader>(array.ValueAt(0));
			ownerVC = viewController;
			return view;
		}

		public static void SetTrip(Trip trip)
		{
			selectedTrip = trip;
		}

		public TableHeader (IntPtr handle) : base (handle)
        {}

		public override void AwakeFromNib()
		{
			PassengersTableView.RegisterNibForCellReuse(PassengersCell.Nib, PassengersCell.Key);
			SetButtonsHandlers();
			UpdateHeader();
		}

		public void UpdateHeader()
		{
			DepartureLabel.Text = selectedTrip.Route.GetDepartureRoute;
			ReturningLabel.Text = selectedTrip.Route.GetReturningRoute;
			OrderNumberLabel.Text = selectedTrip.OrderNumber;
			AirlineRecordLabel.Text = selectedTrip.AirlineRecordLocator;
			PassengersTableView.Source = new PassengersTableSource(selectedTrip.Passengers);

			PassengersTableHeight.Constant = PassengersCell.CellHeight * selectedTrip.Passengers.Count;
			Height = Frame.Height + PassengersTableHeight.Constant - ExtraSpace;
		}

		private void SetButtonsHandlers()
		{
			CancelTripButton.TouchUpInside += (sender, e) =>
			{
				var cancelAlertController = UIAlertController.Create("Trip Canceled",
																	 "Trip has been canceled successfully",
																	 UIAlertControllerStyle.Alert);
				cancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
				ownerVC.PresentViewController(cancelAlertController, true, null);
			};

			ModifyTripButton.TouchUpInside += (sender, e) =>
			{
				var modifyAlertController = UIAlertController.Create("Trip Modified",
																	 "Trip has been modified successfully",
																	 UIAlertControllerStyle.Alert);
				modifyAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
				ownerVC.PresentViewController(modifyAlertController, true, null);
			};
		}
	}
}