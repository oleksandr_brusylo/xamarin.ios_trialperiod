// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Task6
{
    [Register ("TableHeader")]
    partial class TableHeader
    {
        [Outlet]
        UIKit.UILabel AirlineRecordLabel { get; set; }


        [Outlet]
        UIKit.UIButton CancelTripButton { get; set; }


        [Outlet]
        UIKit.UILabel DepartureLabel { get; set; }


        [Outlet]
        UIKit.UIButton ModifyTripButton { get; set; }


        [Outlet]
        UIKit.UILabel OrderNumberLabel { get; set; }


        [Outlet]
        UIKit.NSLayoutConstraint PassengersTableHeight { get; set; }


        [Outlet]
        UIKit.UITableView PassengersTableView { get; set; }


        [Outlet]
        UIKit.UILabel ReturningLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AirlineRecordLabel != null) {
                AirlineRecordLabel.Dispose ();
                AirlineRecordLabel = null;
            }

            if (CancelTripButton != null) {
                CancelTripButton.Dispose ();
                CancelTripButton = null;
            }

            if (DepartureLabel != null) {
                DepartureLabel.Dispose ();
                DepartureLabel = null;
            }

            if (ModifyTripButton != null) {
                ModifyTripButton.Dispose ();
                ModifyTripButton = null;
            }

            if (OrderNumberLabel != null) {
                OrderNumberLabel.Dispose ();
                OrderNumberLabel = null;
            }

            if (PassengersTableHeight != null) {
                PassengersTableHeight.Dispose ();
                PassengersTableHeight = null;
            }

            if (PassengersTableView != null) {
                PassengersTableView.Dispose ();
                PassengersTableView = null;
            }

            if (ReturningLabel != null) {
                ReturningLabel.Dispose ();
                ReturningLabel = null;
            }
        }
    }
}