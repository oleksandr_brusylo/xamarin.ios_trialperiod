﻿using UIKit;
using System;
using Foundation;
using System.Text;
using System.Linq;
using CoreGraphics;
using System.Collections.Generic;

namespace Task6
{
	public partial class TransfersViewController : UIViewController
	{
		public Transfer LastTransfer { get; set; }
		public Transfer CurrentTransfer { get; set; }
		public Transfer NextTransfer { get; set; }
		public TransfersTableSource TableSource { get;set; }

		UIView transparentView;
		WeatherForecast weatherView;

		string baggage = "BAGGAGE";
		string layover = "LAYOVER";

		string formattingDateMask = "ddd, MMM dd";
		string formattingTimeMask = "hh:mm tt";

		nfloat horizMargin = 20;
		nfloat elementHeight = 15;
		float constraintPriority = 999;

		bool transferIsLast;

		public TransfersViewController() : base("TransfersViewController", null)
		{}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			TableSource.UserSelectsRow += SetSelectedTransfer;

			UpdateView();
			SetButtonsHandlers();
			SetRightBarButtonItem();
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			AddBorder();
		}

		private void SetButtonsHandlers() 
		{
			DepartureGeomarkButton.TouchUpInside += (sender, e) => 
			{
				NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
				NavigationController.PushViewController
					(new MapViewController(CurrentTransfer.DeparturePoint, new ImperialMapManager()),true);
			};

			ArrivalGeomarkButton.TouchUpInside += (sender, e) => 
			{
				NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
				NavigationController.PushViewController
					(new MapViewController(CurrentTransfer.ArrivalPoint, new ImperialMapManager()),true);
			};
		}

		private void SetRightBarButtonItem()
		{
			var rightButton = new UIButton(UIButtonType.Custom);
			rightButton.SetImage(UIImage.FromBundle("icon-share-128.png"), UIControlState.Normal);
			rightButton.Frame = new CGRect(0, 0, 30, 30);
			rightButton.TouchUpInside += (sender, e) =>
			{
				var shareAlertController = UIAlertController.Create("Transfer Shared",
				                                                    "Transfer has been shared successfully",
				                                                    UIAlertControllerStyle.Alert);
				shareAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
				PresentViewController(shareAlertController, true, null);
			};

			var rightButtonItem = new UIBarButtonItem(rightButton);
			NavigationItem.SetRightBarButtonItem(rightButtonItem, true);
		}

		private void AddBorder() //TODO:such a crutch
		{
			if (transparentView == null)
			{
				AddViewWithBorder();
			}
			else 
			{
				transparentView.RemoveFromSuperview();
				AddViewWithBorder();
			}
		}

		private void AddViewWithBorder()
		{
			transparentView = new UIView(new CGRect(0, 0, View.Frame.Width - horizMargin, ViewContainer.Frame.Height));
			transparentView.Layer.BorderColor = UIColor.FromRGB(208, 208, 208).CGColor;
			transparentView.Layer.BorderWidth = 1.0f;
			transparentView.UserInteractionEnabled = false;
			ViewContainer.SendSubviewToBack(transparentView);
			ViewContainer.AddSubview(transparentView);
		}

		public void SetSelectedTransfer(object sender, SelectedRowEventArgs args)
		{
			UpdateView();
		}

		public void UpdateView()
		{
			Title = CurrentTransfer.GetRouteShort;
			
			InitializeDepartureViews(CurrentTransfer);

			InitializeArrivalViews(CurrentTransfer);

			SetLayout(CurrentTransfer);

			SetLayover();

			SetForecast();
		}

		private void InitializeDepartureViews(Transfer transfer)
		{
			TransferNumber.Text = transfer.TransferNumber;
			AircompanyLogo.Image = UIImage.FromBundle(transfer.ImagePath);
			AircompanyName.Text = transfer.OperatedBy;
			DeparturePointAcronym.Text = transfer.DeparturePoint.Acronym;
			DeparturePointName.Text = transfer.DeparturePoint.FullName;
			DepartureDate.Text = transfer.DepartureTime.ToString(formattingDateMask);
			DepartureTerminal.Text = transfer.Terminal;
			DepartureGate.Text = transfer.Gate;
			StatusLabel.Text = transfer.Status.ToUpper();
			FillSeatsLabel(transfer);

			//DeparturePointChange(transfer); //TODO:uncomment this line to display new airport name
		}

		private void InitializeArrivalViews(Transfer transfer)
		{
			ArrivalPointAcronym.Text = transfer.ArrivalPoint.Acronym;
			ArrivalPointName.Text = transfer.ArrivalPoint.FullName;
			ArrivalDate.Text = transfer.ArrivalTime.ToString(formattingDateMask);
			ArrivalTime.Text = transfer.ArrivalTime.ToString(formattingTimeMask);
			ArrivalTerminal.Text = transfer.ArrivalTerminal;
			ArrivalGate.Text = transfer.ArrivalGate;
			FlightDuration.Text =
				string.Format("{0}h{1}m", transfer.Duration.ToString("%H"), transfer.Duration.ToString("%m"));
		}

		private void DeparturePointChange(Transfer transfer)
		{
			DeparturePointName.Text = "New long airport name test test test test test test test test test test test";
			var attrString = new NSAttributedString(transfer.DeparturePoint.FullName,
													strikethroughStyle: NSUnderlineStyle.Single);
			ChangedDeparturePointName.AttributedText = attrString;
			DeparturePointName.TextColor = UIColor.Red;
		}

		private void SetLayout(Transfer transfer)
		{
			if (transfer.ChangesExist)
			{
				SetLayoutWithFlightChanges(transfer);
			}
			else
			{
				SetLayoutWithoutFlightChanges(transfer);
			}
		}

		private void SetLayoutWithoutFlightChanges(Transfer transfer)
		{
			flightDelayedMarkHeight.Constant = 0;
			flightDelayedMarkHeight.Priority = constraintPriority;
			FlightDelayedMark.Hidden = true;

			previousDepartureTimeHeight.Constant = 0;
			previousDepartureTimeHeight.Priority = constraintPriority;
			ChangedDepartureTime.Hidden = true;


			DepartureTime.Text = transfer.DepartureTime.ToString(formattingTimeMask);
			DepartureTime.TextColor = UIColor.Black;
		}

		private void SetLayoutWithFlightChanges(Transfer transfer)
		{
			var attrString = new NSAttributedString(transfer.DepartureTime.ToString(formattingTimeMask),
													strikethroughStyle: NSUnderlineStyle.Single);
			ChangedDepartureTime.Font = UIFont.FromName("Arial", 15f);
			ChangedDepartureTime.AttributedText = attrString;
			ArrivalTime.Text = "-";
			LayoverLabel.Text = "-";
			DepartureTime.Text = transfer.ChangedDepartureTime.ToString(formattingTimeMask);
			DepartureTime.TextColor = UIColor.Red;

			flightDelayedMarkHeight.Constant = elementHeight;
			FlightDelayedMark.Hidden = false;

			previousDepartureTimeHeight.Constant = elementHeight;
			ChangedDepartureTime.Hidden = false;
		}

		private void FillSeatsLabel(Transfer transfer)
		{
			var sb = new StringBuilder();
			foreach (var seat in transfer.Seats)
			{
				sb.AppendFormat("{0}, ", seat);
			}
			sb.Length -= 2;
			SeatsLabel.Text = sb.ToString();
		}

		private void SetLayover()
		{
			if (CurrentTransfer.TransferId == LastTransfer.TransferId)
			{
				transferIsLast = true;
				LayoverPlaceholder.Text = baggage;
				LayoverLabel.Text = CurrentTransfer.BaggageCarousel;
			}
			else
			{
				transferIsLast = false;
				LayoverPlaceholder.Text = layover;
				if (!CurrentTransfer.ChangesExist)
					LayoverLabel.Text = TransfersManager.CalculateLayover(CurrentTransfer.ArrivalTime,
																		  NextTransfer.DepartureTime).ToString("HH:mm");
			}
		}

		private void AddWeatherForecastFor(int daysAmount)
		{
			var data = GetWeatherData(daysAmount);

			weatherView = WeatherForecast.Create(data);

			ForecastContainerHeight.Constant = WeatherForecast.TableHeight;

			ForecastContainer.Add(weatherView);

			weatherView.Frame = new CGRect(0, 0, ForecastContainer.Frame.Width, ForecastContainer.Frame.Height);

			weatherView.Reload();

			ForecastContainerHeight.Constant = WeatherForecast.TableHeight;
		}

		private List<Weather> GetWeatherData(int daysAmount)
		{
			return TripsRepository.GetWeatherForecast(CurrentTransfer).Take(daysAmount).ToList();
		}

		private void SetForecast()
		{
			if (transferIsLast) 
			{
				AddWeatherForecastFor(daysAmount: 5);
			} 
			else 
			{
				AddWeatherForecastFor(daysAmount: 1);
			}
		}
	}
}