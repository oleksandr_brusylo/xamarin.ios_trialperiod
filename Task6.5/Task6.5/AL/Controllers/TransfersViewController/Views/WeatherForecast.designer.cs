// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6
{
	[Register ("WeatherForecast")]
	partial class WeatherForecast
	{
		[Outlet]
		UIKit.UITableView WeatherTableView { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint WeatherTableViewHeight { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (WeatherTableView != null) {
				WeatherTableView.Dispose ();
				WeatherTableView = null;
			}

			if (WeatherTableViewHeight != null) {
				WeatherTableViewHeight.Dispose ();
				WeatherTableViewHeight = null;
			}
		}
	}
}
