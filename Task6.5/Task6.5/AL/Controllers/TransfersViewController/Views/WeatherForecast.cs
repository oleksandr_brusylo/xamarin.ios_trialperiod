using UIKit;
using System;
using Foundation;
using ObjCRuntime;
using System.Collections.Generic;

namespace Task6
{
    public partial class WeatherForecast : UIView
    {
        public WeatherForecast (IntPtr handle) : base (handle)
        {}

		public static nfloat TableHeight;
		static List<Weather> items;
		nfloat margin = 10;

		public WeatherTableSource DataSource
		{
			get;
			set;
		}

		public static WeatherForecast Create(List<Weather> weatherItems)
		{
			items = weatherItems;
			var array = NSBundle.MainBundle.LoadNib("WeatherForecast", null, null);
			var view = Runtime.GetNSObject<WeatherForecast>(array.ValueAt(0));
			return view;
		}

		public override void AwakeFromNib()
		{
			Initialize();
		}

		public void Initialize()
		{
			WeatherTableView.RegisterNibForCellReuse(WeatherCustomCell.Nib, WeatherCustomCell.Key);
			WeatherTableView.Source = new WeatherTableSource(items);

			WeatherTableView.EstimatedRowHeight = WeatherCustomCell.CellHeight;
			WeatherTableView.RowHeight = UITableView.AutomaticDimension;

			WeatherTableView.ReloadData();
			WeatherTableView.LayoutIfNeeded();
			TableHeight = WeatherTableView.ContentSize.Height + margin;
		}

		public void Reload()
		{
			WeatherTableView.ReloadData();
			WeatherTableView.LayoutIfNeeded();
			TableHeight = WeatherTableView.ContentSize.Height + margin;
		}
	}
}