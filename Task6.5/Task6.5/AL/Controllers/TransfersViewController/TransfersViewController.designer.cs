// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6
{
	[Register ("TransfersViewController")]
	partial class TransfersViewController
	{
		[Outlet]
		UIKit.UIImageView AircompanyLogo { get; set; }

		[Outlet]
		UIKit.UILabel AircompanyName { get; set; }

		[Outlet]
		UIKit.UILabel ArrivalDate { get; set; }

		[Outlet]
		UIKit.UILabel ArrivalGate { get; set; }

		[Outlet]
		UIKit.UIButton ArrivalGeomarkButton { get; set; }

		[Outlet]
		UIKit.UILabel ArrivalPointAcronym { get; set; }

		[Outlet]
		UIKit.UILabel ArrivalPointName { get; set; }

		[Outlet]
		UIKit.UILabel ArrivalTerminal { get; set; }

		[Outlet]
		UIKit.UILabel ArrivalTime { get; set; }

		[Outlet]
		UIKit.UIView BottomView { get; set; }

		[Outlet]
		UIKit.UILabel ChangedDeparturePointName { get; set; }

		[Outlet]
		UIKit.UILabel ChangedDepartureTime { get; set; }

		[Outlet]
		UIKit.UILabel DepartureDate { get; set; }

		[Outlet]
		UIKit.UILabel DepartureGate { get; set; }

		[Outlet]
		UIKit.UIButton DepartureGeomarkButton { get; set; }

		[Outlet]
		UIKit.UILabel DeparturePointAcronym { get; set; }

		[Outlet]
		UIKit.UILabel DeparturePointName { get; set; }

		[Outlet]
		UIKit.UILabel DepartureTerminal { get; set; }

		[Outlet]
		UIKit.UILabel DepartureTime { get; set; }

		[Outlet]
		UIKit.UIButton FlightDelayedMark { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint flightDelayedMarkHeight { get; set; }

		[Outlet]
		UIKit.UILabel FlightDuration { get; set; }

		[Outlet]
		UIKit.UIView ForecastContainer { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint ForecastContainerHeight { get; set; }

		[Outlet]
		UIKit.UILabel LayoverLabel { get; set; }

		[Outlet]
		UIKit.UILabel LayoverPlaceholder { get; set; }

		[Outlet]
		UIKit.UIView MiddleView { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint previousDeparturePointHeight { get; set; }

		[Outlet]
		UIKit.NSLayoutConstraint previousDepartureTimeHeight { get; set; }

		[Outlet]
		UIKit.UILabel SeatsLabel { get; set; }

		[Outlet]
		UIKit.UILabel StatusLabel { get; set; }

		[Outlet]
		UIKit.UIView TopView { get; set; }

		[Outlet]
		UIKit.UILabel TransferNumber { get; set; }

		[Outlet]
		UIKit.UIScrollView TransfersScrollView { get; set; }

		[Outlet]
		UIKit.UIView ViewContainer { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (AircompanyLogo != null) {
				AircompanyLogo.Dispose ();
				AircompanyLogo = null;
			}

			if (AircompanyName != null) {
				AircompanyName.Dispose ();
				AircompanyName = null;
			}

			if (ArrivalDate != null) {
				ArrivalDate.Dispose ();
				ArrivalDate = null;
			}

			if (ArrivalGate != null) {
				ArrivalGate.Dispose ();
				ArrivalGate = null;
			}

			if (ArrivalGeomarkButton != null) {
				ArrivalGeomarkButton.Dispose ();
				ArrivalGeomarkButton = null;
			}

			if (ArrivalPointAcronym != null) {
				ArrivalPointAcronym.Dispose ();
				ArrivalPointAcronym = null;
			}

			if (ArrivalPointName != null) {
				ArrivalPointName.Dispose ();
				ArrivalPointName = null;
			}

			if (ArrivalTerminal != null) {
				ArrivalTerminal.Dispose ();
				ArrivalTerminal = null;
			}

			if (ArrivalTime != null) {
				ArrivalTime.Dispose ();
				ArrivalTime = null;
			}

			if (BottomView != null) {
				BottomView.Dispose ();
				BottomView = null;
			}

			if (ChangedDeparturePointName != null) {
				ChangedDeparturePointName.Dispose ();
				ChangedDeparturePointName = null;
			}

			if (ChangedDepartureTime != null) {
				ChangedDepartureTime.Dispose ();
				ChangedDepartureTime = null;
			}

			if (DepartureDate != null) {
				DepartureDate.Dispose ();
				DepartureDate = null;
			}

			if (DepartureGate != null) {
				DepartureGate.Dispose ();
				DepartureGate = null;
			}

			if (DepartureGeomarkButton != null) {
				DepartureGeomarkButton.Dispose ();
				DepartureGeomarkButton = null;
			}

			if (DeparturePointAcronym != null) {
				DeparturePointAcronym.Dispose ();
				DeparturePointAcronym = null;
			}

			if (DeparturePointName != null) {
				DeparturePointName.Dispose ();
				DeparturePointName = null;
			}

			if (DepartureTerminal != null) {
				DepartureTerminal.Dispose ();
				DepartureTerminal = null;
			}

			if (DepartureTime != null) {
				DepartureTime.Dispose ();
				DepartureTime = null;
			}

			if (FlightDelayedMark != null) {
				FlightDelayedMark.Dispose ();
				FlightDelayedMark = null;
			}

			if (flightDelayedMarkHeight != null) {
				flightDelayedMarkHeight.Dispose ();
				flightDelayedMarkHeight = null;
			}

			if (FlightDuration != null) {
				FlightDuration.Dispose ();
				FlightDuration = null;
			}

			if (ForecastContainer != null) {
				ForecastContainer.Dispose ();
				ForecastContainer = null;
			}

			if (ForecastContainerHeight != null) {
				ForecastContainerHeight.Dispose ();
				ForecastContainerHeight = null;
			}

			if (LayoverLabel != null) {
				LayoverLabel.Dispose ();
				LayoverLabel = null;
			}

			if (LayoverPlaceholder != null) {
				LayoverPlaceholder.Dispose ();
				LayoverPlaceholder = null;
			}

			if (MiddleView != null) {
				MiddleView.Dispose ();
				MiddleView = null;
			}

			if (previousDeparturePointHeight != null) {
				previousDeparturePointHeight.Dispose ();
				previousDeparturePointHeight = null;
			}

			if (previousDepartureTimeHeight != null) {
				previousDepartureTimeHeight.Dispose ();
				previousDepartureTimeHeight = null;
			}

			if (SeatsLabel != null) {
				SeatsLabel.Dispose ();
				SeatsLabel = null;
			}

			if (StatusLabel != null) {
				StatusLabel.Dispose ();
				StatusLabel = null;
			}

			if (TopView != null) {
				TopView.Dispose ();
				TopView = null;
			}

			if (TransferNumber != null) {
				TransferNumber.Dispose ();
				TransferNumber = null;
			}

			if (TransfersScrollView != null) {
				TransfersScrollView.Dispose ();
				TransfersScrollView = null;
			}

			if (ViewContainer != null) {
				ViewContainer.Dispose ();
				ViewContainer = null;
			}
		}
	}
}
