﻿using System;
using System.Collections.Generic;

namespace Task6
{
	public class TransfersManager
	{
		public static DateTime CalculateLayover(DateTime arrivalTime, DateTime departureTime)
		{
			var layover = departureTime.Subtract(arrivalTime);
			return new DateTime(layover.Ticks);
		}

		public static DateTime CalculateDuration(DateTime departureTime, DateTime arrivalTime)
		{
			var duration = arrivalTime.Subtract(departureTime);
			return new DateTime(duration.Ticks);
		}

		public static string GetStatus(DateTime departureTime, DateTime arrivalTime)
		{
			var status = "";

			if (DateTime.Now >= departureTime.AddHours(-3) && DateTime.Now < departureTime.AddHours(-1))
			{
				status = "Check-In";
			}
			else if (DateTime.Now >= departureTime.AddHours(-1) && DateTime.Now < departureTime)
			{
				status = "Go To Gate";
			}
			else if (DateTime.Now >= departureTime && DateTime.Now < arrivalTime)
			{
				status = "In Progress";
			}
			else if (DateTime.Now >= arrivalTime)
			{
				status = "Landed";
			}
			return status;
		}
	}
}