﻿using MapKit;
using CoreLocation;

namespace Task6
{
	public class MapAnnotation : MKAnnotation
	{

		public override CLLocationCoordinate2D Coordinate 
		{ 
			get { return coordinate; } 
		}

		public override string Title 
		{ 
			get { return title; } 
		}

		public override string Subtitle 
		{ 
			get { return subtitle; } 
		}

		CLLocationCoordinate2D coordinate;
		string title, subtitle;

		public MapAnnotation(CLLocationCoordinate2D coordinate, string title, string subtitle)
		{
			this.coordinate = coordinate;
			this.title = title;
			this.subtitle = subtitle;
		}

		public override void SetCoordinate(CLLocationCoordinate2D value)
		{
			coordinate = value;
		}
	}
}