﻿namespace Task6
{
	public interface IMapManager
	{
		double DistanceToLatitudeDegrees(double distance);
		double DistanceToLongitudeDegrees(double distance, double atLatitude);
	}
}