﻿namespace Task6
{
	public class WeatherManager
	{
		public static string GetWeatherIconPath(string condition)
		{
			string iconPath = "";
			switch (condition) 
			{
				case "Sunny":
					iconPath = "WeatherIcons/weather-icon-sunny.png";
					break;
				case "Cloudy":
					iconPath = "WeatherIcons/weather-icon-cloudy.png";
					break;
				case "Cloudy partly with rain":
					iconPath = "WeatherIcons/weather-icon-cloudy-partly-with-rain.png";
					break;
				case "Rainy":
					iconPath = "WeatherIcons/weather-icon-rainy.png";
					break;
				case "Thundery":
					iconPath = "WeatherIcons/weather-icon-thundery.png";
					break;
				default:
					iconPath = "";
					break;
			}
			return iconPath;
		}
	}
}