﻿using System;

namespace Task6
{
	public class Weather
	{
		public string ForecastTemperature
		{
			get;
			set;
		}

		public DateTime ForecastDate
		{
			get;
			set;
		}

		public string ForecastCondition
		{
			get;
			set;
		}
	}
}