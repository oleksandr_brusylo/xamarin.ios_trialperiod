﻿namespace Task6
{
	public class Airport
	{
		public string Acronym
		{
			get;
			set;
		}

		public string FullName
		{
			get;
			set;
		}

		public string Latitude
		{
			get;
			set;
		}

		public string Longitude
		{
			get;
			set;
		}
	}
}