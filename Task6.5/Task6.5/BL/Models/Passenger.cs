﻿namespace Task6
{
	public class Passenger
	{
		public int PassengerId
		{
			get;
			set;
		}

		public string PassengerNumber
		{
			get;
			set;
		}

		public string FirstName
		{
			get;
			set;
		}

		public string LastName
		{
			get;
			set;
		}

		public string GetFullName
		{
			get
			{
				return string.Format("{0} {1}", FirstName, LastName);
			}
		}
	}
}