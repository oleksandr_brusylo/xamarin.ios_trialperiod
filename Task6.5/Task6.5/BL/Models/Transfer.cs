﻿using System;
using System.Collections.Generic;

namespace Task6
{
	public class Transfer
	{
		public int TransferId
		{
			get;set;
		}

		public string TransferNumber
		{
			get;set;
		}

		public DateTime Layover
		{
			get;set;
		}

		public string Status	
		{
			get;set;
		}

		public DateTime DepartureTime
		{
			get;set;
		}

		public DateTime ChangedDepartureTime
		{
			get;set;
		}

		public DateTime ArrivalTime
		{
			get;set;
		}

		public DateTime Duration
		{
			get;set;
		}

		public List<string> Seats
		{
			get;set;
		}

		public string Gate
		{
			get;set;
		}

		public string ArrivalGate
		{
			get;
			set;
		}

		public string ArrivalTerminal
		{
			get;
			set;
		}

		public string Terminal
		{
			get;set;
		}

		public string OperatedBy
		{
			get;set;
		}

		public Airport DeparturePoint
		{
			get;set;
		}

		public Airport ArrivalPoint
		{
			get;set;
		}

		public string ImagePath
		{
			get;
			set;
		}

		public bool ChangesExist
		{
			get;
			set;
		}

		public string BaggageCarousel
		{
			get;
			set;
		}

		public string GetRouteFull
		{
			get
			{
				return string.Format("{0}({1}) - {2}({3})",
				                     DeparturePoint.FullName, 
				                     DeparturePoint.Acronym, 
				                     ArrivalPoint.FullName, 
				                     ArrivalPoint.Acronym);
			}
		}

		public string GetRouteShort
		{
			get 
			{
				return string.Format("{0} - {1}", DeparturePoint.FullName, ArrivalPoint.FullName);
			}
		}
	}
}