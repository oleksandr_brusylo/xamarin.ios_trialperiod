﻿using System;
using System.Collections.Generic;

namespace Task6
{
	public class Trip
	{
		public int TripId
		{
			get;
			set;
		}

		public DateTime DepartureDate
		{
			get;
			set;
		}

		public DateTime ReturnDate
		{
			get;
			set;
		}

		public Route Route
		{
			get;
			set;
		}

		public string OrderNumber
		{
			get;
			set;
		}

		public string AirlineRecordLocator
		{
			get;
			set;
		}

		public List<Passenger> Passengers
		{
			get;
			set;
		}

		public List<Leg> Legs
		{
			get;
			set;
		}
	}
}