﻿namespace Task6
{
	public class Route
	{
		public Route()
		{
		}

		public int RouteId
		{
			get;
			set;
		}

		public string DeparturePoint
		{
			get;
			set;
		}

		public string ArrivalPoint
		{
			get;
			set;
		}

		public string ReturningDeparturePoint
		{
			get;
			set;
		}

		public string ReturningArrivalPoint
		{
			get;
			set;
		}

		public string GetDepartureRoute
		{
			get 
			{
				return string.Format("{0} - {1},", DeparturePoint, ArrivalPoint);
			}
		}

		public string GetReturningRoute
		{
			get
			{
				return string.Format("{0} - {1}", ReturningDeparturePoint, ReturningArrivalPoint);
			}
		}

		public string GetFullRoute
		{
			get 
			{
				return string.Format("{0} - {1}, {2} - {3}", 
				                     DeparturePoint, 
				                     ArrivalPoint, 
				                     ReturningDeparturePoint, 
				                     ReturningArrivalPoint);
			}
		}
	}
}