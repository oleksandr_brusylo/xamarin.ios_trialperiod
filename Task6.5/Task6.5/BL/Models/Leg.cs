﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Task6
{
	public class Leg
	{
		public int LegId
		{
			get;
			set;
		}

		public List<Transfer> Transfers
		{
			get;
			set;
		}

		public Airport DeparturePoint
		{
			get;
			set;
		}

		public Airport ArrivalPoint
		{
			get;
			set;
		}

		public DateTime DepartureTime
		{
			get;
			set;
		}

		public DateTime ArrivalTime
		{
			get;
			set;
		}

		public DateTime TravelTime
		{
			get 
			{
				var delta = ArrivalTime.Subtract(DepartureTime);
				return new DateTime(delta.Ticks);
			}
		}

		public Transfer GetLastTransfer
		{
			get 
			{
				return Transfers.Last();
			}
		}
	}
}