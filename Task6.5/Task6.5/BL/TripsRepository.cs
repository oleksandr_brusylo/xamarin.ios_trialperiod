﻿using System;
using System.Collections.Generic;

namespace Task6
{
	public class TripsRepository
	{
		DateTime TransferOneDepartureTime = DateTime.Now.AddDays(-2);
		DateTime TransferOneArrivalTime = DateTime.Now.AddDays(-2).AddHours(5);

		DateTime TransferTwoDepartureTime = DateTime.Now.AddDays(-1);
		DateTime TransferTwoArrivalTime = DateTime.Now.AddDays(-1).AddHours(5);

		DateTime TransferThreeDepartureTime = DateTime.Now.AddHours(1); //TODO:affects flight status (<1, =1, >1, >3) 
		DateTime TransferThreeArrivalTime = DateTime.Now.AddHours(5);

		DateTime TransferFourDepartureTime = DateTime.Now.AddDays(1);
		DateTime TransferFourArrivalTime = DateTime.Now.AddDays(1).AddHours(5);

		DateTime TransferFiveDepartureTime = DateTime.Now.AddDays(2);
		DateTime TransferFiveArrivalTime = DateTime.Now.AddDays(2).AddHours(5);

		DateTime TransferSixDepartureTime = DateTime.Now.AddDays(3);
		DateTime TransferSixArrivalTime = DateTime.Now.AddDays(3).AddHours(5);

		DateTime TransferSevenDepartureTime = DateTime.Now.AddDays(4);
		DateTime TransferSevenArrivalTime = DateTime.Now.AddDays(4).AddHours(5);

		DateTime TransferEightDepartureTime = DateTime.Now.AddDays(-7);
		DateTime TransferEightArrivalTime = DateTime.Now.AddDays(-7).AddHours(3);

		DateTime TransferNineDepartureTime = DateTime.Now.AddDays(-7).AddHours(5);
		DateTime TransferNineArrivalTime = DateTime.Now.AddDays(-7).AddHours(7);

		DateTime TransferTenDepartureTime = DateTime.Now.AddDays(-7).AddHours(10);
		DateTime TransferTenArrivalTime = DateTime.Now.AddDays(-7).AddHours(15);

		DateTime TransferElevenDepartureTime = DateTime.Now.AddDays(-7).AddHours(20);
		DateTime TransferElevenArrivalTime = DateTime.Now.AddDays(-7).AddHours(24);

		DateTime TransferTwelveDepartureTime = DateTime.Now.AddDays(-5);
		DateTime TransferTwelveArrivalTime = DateTime.Now.AddDays(-5).AddHours(5);

		DateTime TransferThirteenDepartureTime = DateTime.Now.AddDays(-5).AddHours(7);
		DateTime TransferThirteenArrivalTime = DateTime.Now.AddDays(-5).AddHours(10);
		
		public TripsRepository()
		{
			Trips = new List<Trip>()
			{
				new Trip()
				{
					TripId = 1,
					Route = new Route()
					{
						RouteId = 1,
						DeparturePoint = "Kiev",
						ArrivalPoint = "Las Vegas",
						ReturningDeparturePoint = "San Diego",
						ReturningArrivalPoint = "Kiev"
					},
					DepartureDate = TransferOneDepartureTime,
					ReturnDate = TransferSevenArrivalTime,
					OrderNumber = "O-2B13740",
					AirlineRecordLocator = "E2XCKS",
					Passengers = new List<Passenger>()
					{
						new Passenger()
						{
							PassengerId = 1,
							FirstName = "John",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						},
						new Passenger()
						{
							PassengerId = 2,
							FirstName = "Margaret",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						},
						new Passenger()
						{
							PassengerId = 3,
							FirstName = "Michael",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						},
						new Passenger()
						{
							PassengerId = 4,
							FirstName = "Anna",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						},
						new Passenger()
						{
							PassengerId = 5,
							FirstName = "Alex",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						},
						new Passenger()
						{
							PassengerId = 6,
							FirstName = "Alexandra",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						},
						new Passenger()
						{
							PassengerId = 7,
							FirstName = "Barbara",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						}
					},
					Legs = new List<Leg>()
					{
						new Leg()
						{
							LegId = 1,
							DeparturePoint = new Airport ()
							{
								Acronym = "KBP",
								FullName = "KIEV"
							},
							ArrivalPoint = new Airport ()
							{
								Acronym = "LAS",
								FullName = "LAS VEGAS"
							},
							DepartureTime = TransferOneDepartureTime,
							ArrivalTime = TransferThreeArrivalTime,
							Transfers = new List<Transfer>()
							{
								new Transfer()
								{
									TransferId = 1,
									DeparturePoint = new Airport()
									{
										Acronym = "KBP",
										FullName = "Kiev",
										Latitude = "50.338",
										Longitude = "30.893"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									DepartureTime = TransferOneDepartureTime,
									ArrivalTime = TransferOneArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL5307",
									Status = TransfersManager.GetStatus(TransferOneDepartureTime,
																		TransferOneArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferOneDepartureTime,
																				  TransferOneArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferOneArrivalTime,
																				TransferTwoDepartureTime),
									OperatedBy = "DELTA AIRLINES",
									Gate = "37",
									ArrivalGate = "37",
									Terminal = "D",
									ArrivalTerminal = "D",
									Seats = new List<string>()
									{
										"25A", "25B"
									},
									ImagePath = "Logos/deltaairlines-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 2,
									DeparturePoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "JFK",
										FullName = "New York",
										Latitude = "40.643",
										Longitude = "-73.782"
									},
									DepartureTime = TransferTwoDepartureTime,
									ArrivalTime = TransferTwoArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL5353",
									Status = TransfersManager.GetStatus(TransferTwoDepartureTime,
																		TransferTwoArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferTwoDepartureTime,
																				  TransferTwoArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferTwoArrivalTime,
																				TransferThreeDepartureTime),
									OperatedBy = "LUFTHANSA",
									Gate = "38",
									ArrivalGate = "38",
									Terminal = "F",
									ArrivalTerminal = "F",
									Seats = new List<string>()
									{
										"99A", "99B"
									},
									ImagePath = "Logos/lufthansa-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 3,
									DeparturePoint = new Airport()
									{
										Acronym = "JFK",
										FullName = "New York",
										Latitude = "40.643",
										Longitude = "-73.782"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "LAS",
										FullName = "Las Vegas",
										Latitude = "36.087",
										Longitude = "-115.138"
									},
									DepartureTime = TransferThreeDepartureTime,
									ArrivalTime = TransferThreeArrivalTime,
									ChangedDepartureTime = TransferThreeDepartureTime.AddMinutes(30),
									TransferNumber = "KL5309",
									Status = TransfersManager.GetStatus(TransferThreeDepartureTime,
																		TransferThreeArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferThreeDepartureTime,
																				  TransferThreeArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferThreeArrivalTime,
									                                            TransferThreeArrivalTime),
									OperatedBy = "EMIRATES AIRLINE",
									Gate = "39",
									ArrivalGate = "39",
									Terminal = "A",
									ArrivalTerminal = "A",
									Seats = new List<string>()
									{
										"50A", "50B"
									},
									ImagePath = "Logos/emiratesairline-logo.png",
									ChangesExist = true,
									BaggageCarousel = "№11"
								}
							}
						},
						new Leg()
						{
							LegId = 2,
							DeparturePoint = new Airport ()
							{
								Acronym = "SAN",
								FullName = "SAN DIEGO"
							},
							ArrivalPoint = new Airport ()
							{
								Acronym = "KBP",
								FullName = "KIEV"
							},
							DepartureTime = TransferFourDepartureTime,
							ArrivalTime = TransferSevenArrivalTime,
							Transfers = new List<Transfer>()
							{
								new Transfer()
								{
									TransferId = 4,
									DeparturePoint = new Airport()
									{
										Acronym = "SAN",
										FullName = "San Diego",
										Latitude = "32.730",
										Longitude = "-117.199"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "SEA",
										FullName = "Seattle",
										Latitude = "47.450",
										Longitude = "-122.308"
									},
									DepartureTime = TransferFourDepartureTime,
									ArrivalTime = TransferFourArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL6720",
									Status = TransfersManager.GetStatus(TransferFourDepartureTime,
																		TransferFourArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferFourDepartureTime,
																				  TransferFourArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferFourArrivalTime,
																				TransferFiveDepartureTime),
									OperatedBy = "AIR CHINA",
									Gate = "14",
									ArrivalGate = "14",
									Terminal = "F",
									ArrivalTerminal = "F",
									Seats = new List<string>()
									{
										"11A", "11B"
									},
									ImagePath = "Logos/airchina-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 5,
									DeparturePoint = new Airport()
									{
										Acronym = "SEA",
										FullName = "Seattle",
										Latitude = "47.450",
										Longitude = "-122.308"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									DepartureTime = TransferFiveDepartureTime,
									ArrivalTime = TransferFiveArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL6740",
									Status = TransfersManager.GetStatus(TransferFiveDepartureTime,
																		TransferFiveArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferFiveDepartureTime,
																				  TransferFiveArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferFiveArrivalTime,
																				TransferSixDepartureTime),
									OperatedBy = "TURKISH AIRLINES",
									Gate = "40",
									ArrivalGate = "40",
									Terminal = "B",
									ArrivalTerminal = "B",
									Seats = new List<string>()
									{
										"22A", "22B"
									},
									ImagePath = "Logos/turkishairlines-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 6,
									DeparturePoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "KBP",
										FullName = "Kiev",
										Latitude = "50.338",
										Longitude = "30.893"
									},
									DepartureTime = TransferSixDepartureTime,
									ArrivalTime = TransferSixArrivalTime,
									ChangedDepartureTime = TransferSixDepartureTime.AddHours(2),
									TransferNumber = "KL6730",
									Status = TransfersManager.GetStatus(TransferSixDepartureTime,
									                                    TransferSixArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferSixDepartureTime,
																				  TransferSixArrivalTime),

									Layover = TransfersManager.CalculateLayover(TransferSixArrivalTime,
									                                            TransferSevenDepartureTime),
									OperatedBy = "AEROSVIT UKRAINE",
									Gate = "25",
									ArrivalGate = "25",
									Terminal = "A",
									ArrivalTerminal = "A",
									Seats = new List<string>()
									{
										"05A", "05B"
									},
									ImagePath = "Logos/aerosvit-logo.png",
									ChangesExist = true,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 7,
									DeparturePoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									DepartureTime = TransferSevenDepartureTime,
									ArrivalTime = TransferSevenArrivalTime,
									ChangedDepartureTime = TransferSevenDepartureTime.AddMinutes(45),
									TransferNumber = "TEST",
									Status = TransfersManager.GetStatus(TransferSevenDepartureTime,
									                                    TransferSevenArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferSevenDepartureTime,
									                                              TransferSevenArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferSevenArrivalTime,
									                                            TransferSevenArrivalTime),
									OperatedBy = "TEST",
									Gate = "45",
									ArrivalGate = "45",
									Terminal = "G",
									ArrivalTerminal = "G",
									Seats = new List<string>()
									{
										"15A", "15B", "15C", "15D", "15E", "15F", "15G", "15H", "15I", "15J", "15K",
										"15L", "15M", "15N", "15O", "15P", "15Q", "15R", "15S", "15T", "15U", "15V",
										"15W", "15X", "15Y", "15Z"
									},
									ImagePath = "",
									ChangesExist = true,
									BaggageCarousel = "№25"
								}
							}
						}
					}
				},
				new Trip()
				{
					TripId = 2,
					Route = new Route()
					{
						RouteId = 2,
						DeparturePoint = "Kiev",
						ArrivalPoint = "New York",
						ReturningDeparturePoint = "New York",
						ReturningArrivalPoint = "Kiev"
					},
					DepartureDate = TransferEightDepartureTime,
					ReturnDate = TransferThirteenArrivalTime,
					OrderNumber = "IDDQD153270",
					AirlineRecordLocator = "XCABCZXABCZ",
					Passengers = new List<Passenger>()
					{
						new Passenger()
						{
							PassengerId = 8,
							FirstName = "John",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"

						},
						new Passenger()
						{
							PassengerId = 9,
							FirstName = "Margaret",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						}
					},
					Legs = new List<Leg>()
					{
						new Leg()
						{
							LegId = 3,
							DeparturePoint = new Airport ()
							{
								Acronym = "KBP",
								FullName = "KIEV"
							},
							ArrivalPoint = new Airport ()
							{
								Acronym = "AMS",
								FullName = "AMSTERDAM"
							},
							DepartureTime = TransferEightDepartureTime,
							ArrivalTime = TransferElevenArrivalTime,
							Transfers = new List<Transfer>()
							{
								new Transfer()
								{
									TransferId = 9,
									DeparturePoint = new Airport()
									{
										Acronym = "SEA",
										FullName = "Seattle",
										Latitude = "47.450",
										Longitude = "-122.308"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.7725"
									},
									DepartureTime = TransferNineDepartureTime,
									ArrivalTime = TransferNineArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL6740",
									Status = TransfersManager.GetStatus(TransferNineDepartureTime,
																		TransferNineArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferNineDepartureTime,
																				  TransferNineArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferNineArrivalTime,
																				TransferTenDepartureTime),
									OperatedBy = "TURKISH AIRLINES",
									Gate = "40",
									ArrivalGate = "40",
									Terminal = "B",
									ArrivalTerminal = "B",
									Seats = new List<string>()
									{
										"22A", "22B"
									},
									ImagePath = "Logos/turkishairlines-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 10,
									DeparturePoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "KBP",
										FullName = "Kiev",
										Latitude = "50.338",
										Longitude = "30.893"
									},
									DepartureTime = TransferTenDepartureTime,
									ArrivalTime = TransferTenArrivalTime,
									ChangedDepartureTime = TransferTenDepartureTime.AddHours(2),
									TransferNumber = "KL6730",
									Status = TransfersManager.GetStatus(TransferTenDepartureTime,
																		TransferTenArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferTenDepartureTime,
																				  TransferTenArrivalTime),

									Layover = TransfersManager.CalculateLayover(TransferTenArrivalTime,
																				TransferElevenDepartureTime),
									OperatedBy = "AEROSVIT UKRAINE",
									Gate = "25",
									ArrivalGate = "25",
									Terminal = "A",
									ArrivalTerminal = "A",
									Seats = new List<string>()
									{
										"05A", "05B"
									},
									ImagePath = "Logos/aerosvit-logo.png",
									ChangesExist = true,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 11,
									DeparturePoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									DepartureTime = TransferElevenDepartureTime,
									ArrivalTime = TransferElevenArrivalTime,
									ChangedDepartureTime = TransferElevenDepartureTime.AddMinutes(45),
									TransferNumber = "TEST",
									Status = TransfersManager.GetStatus(TransferElevenDepartureTime,
																		TransferElevenArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferElevenDepartureTime,
																				  TransferElevenArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferElevenArrivalTime,
																				TransferElevenArrivalTime),
									OperatedBy = "TEST",
									Gate = "45",
									ArrivalGate = "45",
									Terminal = "G",
									ArrivalTerminal = "G",
									Seats = new List<string>()
									{
										"15A", "15B", "15C", "15D", "15E", "15F", "15G", "15H", "15I", "15J", "15K",
										"15L", "15M", "15N", "15O", "15P", "15Q", "15R", "15S", "15T", "15U", "15V",
										"15W", "15X", "15Y", "15Z"
									},
									ImagePath = "",
									ChangesExist = true,
									BaggageCarousel = "№800"
								}
							}
						},
						new Leg()
						{
							LegId = 4,
							DeparturePoint = new Airport ()
							{
								Acronym = "BUD",
								FullName = "BUDAPEST"
							},
							ArrivalPoint = new Airport ()
							{
								Acronym = "JFK",
								FullName = "NEW YORK"
							},
							DepartureTime = TransferTwelveDepartureTime,
							ArrivalTime = TransferThirteenArrivalTime,
							Transfers = new List<Transfer>()
							{
								new Transfer()
								{
									TransferId = 12,
									DeparturePoint = new Airport()
									{
										Acronym = "BUD",
										FullName = "Budapest",
										Latitude = "47.438",
										Longitude = "19.252"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									DepartureTime = TransferTwelveDepartureTime,
									ArrivalTime = TransferTwelveArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL1800",
									Status = TransfersManager.GetStatus(TransferTwelveDepartureTime,
									                                    TransferTwelveArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferTwelveDepartureTime,
									                                              TransferTwelveArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferTwelveArrivalTime,
																				TransferThirteenDepartureTime),
									OperatedBy = "TURKISH AIRLINES",
									Gate = "40",
									ArrivalGate = "40",
									Terminal = "B",
									ArrivalTerminal = "B",
									Seats = new List<string>()
									{
										"22A", "22B"
									},
									ImagePath = "Logos/turkishairlines-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 13,
									DeparturePoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "JFK",
										FullName = "New York",
										Latitude = "40.6438",
										Longitude = "-73.782"
									},
									DepartureTime = TransferThirteenDepartureTime,
									ArrivalTime = TransferThirteenArrivalTime,
									ChangedDepartureTime = TransferThirteenDepartureTime.AddHours(2),
									TransferNumber = "KL228",
									Status = TransfersManager.GetStatus(TransferThirteenDepartureTime,
																		TransferThirteenArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferThirteenDepartureTime,
																				  TransferThirteenArrivalTime),

									Layover = TransfersManager.CalculateLayover(TransferThirteenArrivalTime,
																				TransferThirteenArrivalTime),
									OperatedBy = "AEROSVIT UKRAINE",
									Gate = "25",
									ArrivalGate = "25",
									Terminal = "A",
									ArrivalTerminal = "A",
									Seats = new List<string>()
									{
										"05A", "05B"
									},
									ImagePath = "Logos/aerosvit-logo.png",
									ChangesExist = true,
									BaggageCarousel = "900"
								}			
							}
						}
					}
				},
				new Trip()
				{
					TripId = 3,
					Route = new Route()
					{
						RouteId = 1,
						DeparturePoint = "Kiev",
						ArrivalPoint = "Las Vegas",
						ReturningDeparturePoint = "San Diego",
						ReturningArrivalPoint = "Kiev"
					},
					DepartureDate = TransferOneDepartureTime,
					ReturnDate = TransferSevenArrivalTime,
					OrderNumber = "O-2B13740",
					AirlineRecordLocator = "E2XCKS",
					Passengers = new List<Passenger>()
					{
						new Passenger()
						{
							PassengerId = 1,
							FirstName = "John",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						},
						new Passenger()
						{
							PassengerId = 2,
							FirstName = "Margaret",
							LastName = "Smith",
							PassengerNumber = "(76-7101592203)"
						}
					},
					Legs = new List<Leg>()
					{
						new Leg()
						{
							LegId = 1,
							DeparturePoint = new Airport ()
							{
								Acronym = "KBP",
								FullName = "KIEV"
							},
							ArrivalPoint = new Airport ()
							{
								Acronym = "LAS",
								FullName = "LAS VEGAS"
							},
							DepartureTime = TransferOneDepartureTime,
							ArrivalTime = TransferThreeArrivalTime,
							Transfers = new List<Transfer>()
							{
								new Transfer()
								{
									TransferId = 1,
									DeparturePoint = new Airport()
									{
										Acronym = "KBP",
										FullName = "Kiev",
										Latitude = "50.338",
										Longitude = "30.893"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									DepartureTime = TransferOneDepartureTime,
									ArrivalTime = TransferOneArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL5307",
									Status = TransfersManager.GetStatus(TransferOneDepartureTime,
																		TransferOneArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferOneDepartureTime,
																				  TransferOneArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferOneArrivalTime,
																				TransferTwoDepartureTime),
									OperatedBy = "DELTA AIRLINES",
									Gate = "37",
									ArrivalGate = "37",
									Terminal = "D",
									ArrivalTerminal = "D",
									Seats = new List<string>()
									{
										"25A", "25B"
									},
									ImagePath = "Logos/deltaairlines-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 2,
									DeparturePoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "JFK",
										FullName = "New York",
										Latitude = "40.643",
										Longitude = "-73.782"
									},
									DepartureTime = TransferTwoDepartureTime,
									ArrivalTime = TransferTwoArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL5353",
									Status = TransfersManager.GetStatus(TransferTwoDepartureTime,
																		TransferTwoArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferTwoDepartureTime,
																				  TransferTwoArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferTwoArrivalTime,
																				TransferThreeDepartureTime),
									OperatedBy = "LUFTHANSA",
									Gate = "38",
									ArrivalGate = "38",
									Terminal = "F",
									ArrivalTerminal = "F",
									Seats = new List<string>()
									{
										"99A", "99B"
									},
									ImagePath = "Logos/lufthansa-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
							}
						},
						new Leg()
						{
							LegId = 2,
							DeparturePoint = new Airport ()
							{
								Acronym = "SAN",
								FullName = "SAN DIEGO"
							},
							ArrivalPoint = new Airport ()
							{
								Acronym = "KBP",
								FullName = "KIEV"
							},
							DepartureTime = TransferFourDepartureTime,
							ArrivalTime = TransferSevenArrivalTime,
							Transfers = new List<Transfer>()
							{
								new Transfer()
								{
									TransferId = 4,
									DeparturePoint = new Airport()
									{
										Acronym = "SAN",
										FullName = "San Diego",
										Latitude = "32.730",
										Longitude = "-117.199"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "SEA",
										FullName = "Seattle",
										Latitude = "47.450",
										Longitude = "-122.308"
									},
									DepartureTime = TransferFourDepartureTime,
									ArrivalTime = TransferFourArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL6720",
									Status = TransfersManager.GetStatus(TransferFourDepartureTime,
																		TransferFourArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferFourDepartureTime,
																				  TransferFourArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferFourArrivalTime,
																				TransferFiveDepartureTime),
									OperatedBy = "AIR CHINA",
									Gate = "14",
									ArrivalGate = "14",
									Terminal = "F",
									ArrivalTerminal = "F",
									Seats = new List<string>()
									{
										"11A", "11B"
									},
									ImagePath = "Logos/airchina-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 5,
									DeparturePoint = new Airport()
									{
										Acronym = "SEA",
										FullName = "Seattle",
										Latitude = "47.450",
										Longitude = "-122.308"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									DepartureTime = TransferFiveDepartureTime,
									ArrivalTime = TransferFiveArrivalTime,
									ChangedDepartureTime = default(DateTime),
									TransferNumber = "KL6740",
									Status = TransfersManager.GetStatus(TransferFiveDepartureTime,
																		TransferFiveArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferFiveDepartureTime,
																				  TransferFiveArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferFiveArrivalTime,
																				TransferSixDepartureTime),
									OperatedBy = "TURKISH AIRLINES",
									Gate = "40",
									ArrivalGate = "40",
									Terminal = "B",
									ArrivalTerminal = "B",
									Seats = new List<string>()
									{
										"22A", "22B"
									},
									ImagePath = "Logos/turkishairlines-logo.png",
									ChangesExist = false,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 6,
									DeparturePoint = new Airport()
									{
										Acronym = "AMS",
										FullName = "Amsterdam",
										Latitude = "52.313",
										Longitude = "4.772"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "KBP",
										FullName = "Kiev",
										Latitude = "50.338",
										Longitude = "30.893"
									},
									DepartureTime = TransferSixDepartureTime,
									ArrivalTime = TransferSixArrivalTime,
									ChangedDepartureTime = TransferSixDepartureTime.AddHours(2),
									TransferNumber = "KL6730",
									Status = TransfersManager.GetStatus(TransferSixDepartureTime,
																		TransferSixArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferSixDepartureTime,
																				  TransferSixArrivalTime),

									Layover = TransfersManager.CalculateLayover(TransferSixArrivalTime,
																				TransferSevenDepartureTime),
									OperatedBy = "AEROSVIT UKRAINE",
									Gate = "25",
									ArrivalGate = "25",
									Terminal = "A",
									ArrivalTerminal = "A",
									Seats = new List<string>()
									{
										"05A", "05B"
									},
									ImagePath = "Logos/aerosvit-logo.png",
									ChangesExist = true,
									BaggageCarousel = "-"
								},
								new Transfer()
								{
									TransferId = 7,
									DeparturePoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									DepartureTime = TransferSevenDepartureTime,
									ArrivalTime = TransferSevenArrivalTime,
									ChangedDepartureTime = TransferSevenDepartureTime.AddMinutes(45),
									TransferNumber = "TEST",
									Status = TransfersManager.GetStatus(TransferSevenDepartureTime,
																		TransferSevenArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferSevenDepartureTime,
																				  TransferSevenArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferSevenArrivalTime,
																				TransferSevenArrivalTime),
									OperatedBy = "TEST",
									Gate = "45",
									ArrivalGate = "45",
									Terminal = "G",
									ArrivalTerminal = "G",
									Seats = new List<string>()
									{
										"15A", "15B", "15C", "15D", "15E", "15F", "15G", "15H", "15I", "15J", "15K",
										"15L", "15M", "15N", "15O", "15P", "15Q", "15R", "15S", "15T", "15U", "15V",
										"15W", "15X", "15Y", "15Z"
									},
									ImagePath = "",
									ChangesExist = true,
									BaggageCarousel = "№25"
								},
								new Transfer()
								{
									TransferId = 8,
									DeparturePoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTESTTEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									ArrivalPoint = new Airport()
									{
										Acronym = "TEST",
										FullName = "TEST",
										Latitude = "49.065",
										Longitude = "33.41"
									},
									DepartureTime = TransferSevenDepartureTime,
									ArrivalTime = TransferSevenArrivalTime,
									ChangedDepartureTime = TransferSevenDepartureTime.AddMinutes(45),
									TransferNumber = "TEST",
									Status = TransfersManager.GetStatus(TransferSevenDepartureTime,
																		TransferSevenArrivalTime),
									Duration = TransfersManager.CalculateDuration(TransferSevenDepartureTime,
																				  TransferSevenArrivalTime),
									Layover = TransfersManager.CalculateLayover(TransferSevenArrivalTime,
																				TransferSevenArrivalTime),
									OperatedBy = "TEST",
									Gate = "45",
									ArrivalGate = "45",
									Terminal = "G",
									ArrivalTerminal = "G",
									Seats = new List<string>()
									{
										"15A", "15B", "15C", "15D", "15E", "15F", "15G", "15H", "15I", "15J", "15K",
										"15L", "15M", "15N", "15O", "15P", "15Q", "15R", "15S", "15T", "15U", "15V",
										"15W", "15X", "15Y", "15Z"
									},
									ImagePath = "",
									ChangesExist = true,
									BaggageCarousel = "№25"
								}
							}
						}
					}
				}
			};
		}

		public List<Trip> Trips
		{
			get;
			set;
		}

		public static List<Weather> GetWeatherForecast(Transfer transfer)
		{
			return new List<Weather>()
			{
				new Weather()
				{
					ForecastDate = transfer.ArrivalTime.AddDays(1),
					ForecastCondition = "Sunny",
					ForecastTemperature = "17°"
				},
				new Weather()
				{
					ForecastDate = transfer.ArrivalTime.AddDays(2),
					ForecastCondition = "Cloudy",
					ForecastTemperature = "20°"
				},
				new Weather()
				{
					ForecastDate = transfer.ArrivalTime.AddDays(3),
					ForecastCondition = "Cloudy partly with rain",
					ForecastTemperature = "20°"
				},
				new Weather()
				{
					ForecastDate = transfer.ArrivalTime.AddDays(4),
					ForecastCondition = "Rainy",
					ForecastTemperature = "15°"
				},
				new Weather()
				{
					ForecastDate = transfer.ArrivalTime.AddDays(5),
					ForecastCondition = "Thundery",
					ForecastTemperature = "10°"
				}
			};
		}
	}
}