﻿using System;
using System.Collections.Generic;
using UIKit;
using Foundation;

namespace Task6
{
public class RootTableSource : UITableViewSource
	{
		List<Employee> tableItems;
		MasterViewController masterVC;
		DetailViewController detailVC;
		string cellIdentifier = "employeecell";

		public event EventHandler<SelectedRowEventArgs> UserSelectsRow;

		public RootTableSource(List<Employee> items, MasterViewController masterVC, DetailViewController detailVC)
		{
			tableItems = items;
			this.masterVC = masterVC;
			this.detailVC = detailVC;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 45f;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return tableItems.Count;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			masterVC.NavigationController.PushViewController(detailVC, true);
			OnUserSelectedRow(new SelectedRowEventArgs(indexPath.Row));
		}

		protected virtual void OnUserSelectedRow(SelectedRowEventArgs e)
		{
			EventHandler<SelectedRowEventArgs> handler = UserSelectsRow;

			if (handler != null)
			{
				handler(this, e);
			}
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(cellIdentifier) as EmployeeCustomCell;
			if (cell == null)
			{
				cell = new EmployeeCustomCell((NSString)cellIdentifier);
			}
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.UpdateCell(tableItems[indexPath.Row].FullName,
							tableItems[indexPath.Row].HiringDate,
							tableItems[indexPath.Row].Position);
			return cell;
		}

		public Employee GetItem(int id)
		{
			return tableItems[id];
		}

		public override void CommitEditingStyle(UITableView tableView,
										UITableViewCellEditingStyle editingStyle,
										NSIndexPath indexPath)
		{
			switch (editingStyle)
			{
				case UITableViewCellEditingStyle.Delete:
					tableItems.RemoveAt(indexPath.Row);
					tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
					break;
				case UITableViewCellEditingStyle.None:
					break;
			}
		}

		public override string TitleForDeleteConfirmation(UITableView tableView, NSIndexPath indexPath)
		{
			return "Delete " + string.Format("{0}\n({1})",
											 tableItems[indexPath.Row].FullName,
											 tableItems[indexPath.Row].Position) + " ?";
		}
	}
}