﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace Task6
{
public class EmployeeCustomCell : UITableViewCell
	{
		UILabel fullNameLabel,
				positionLabel,
				hiringDateLabel;

		public EmployeeCustomCell(NSString cellId) : base(UITableViewCellStyle.Default, cellId)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Gray;
			ContentView.BackgroundColor = UIColor.White;
			fullNameLabel = InitializeLabel(true, UITextAlignment.Left);
			positionLabel = InitializeLabel(false, UITextAlignment.Left);
			hiringDateLabel = InitializeLabel(false, UITextAlignment.Right);
			ContentView.AddSubviews(new UIView[] { fullNameLabel, positionLabel, hiringDateLabel });
		}

		public void UpdateCell(string fullName, DateTime hiringDate, string position)
		{
			fullNameLabel.Text = fullName;
			positionLabel.Text = position;
			hiringDateLabel.Text = hiringDate.ToShortDateString();
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();
			fullNameLabel.Frame = new CGRect(5, 5, ContentView.Bounds.Width - 50, 30);
			positionLabel.Frame = new CGRect(5, 30, ContentView.Bounds.Width / 2, 15);
			hiringDateLabel.Frame = new CGRect(ContentView.Bounds.Width / 2, 25, ContentView.Bounds.Width / 2, 15);
		}

		private UILabel InitializeLabel(bool isTitle, UITextAlignment alignment)
		{
			var color = UIColor.Black;
			var fontSize = 18f;
			if (!isTitle)
			{
				color = UIColor.Gray;
				fontSize = 12f;
			}
			return new UILabel()
			{
				Font = UIFont.FromName("Verdana", fontSize),
				TextColor = color,
				TextAlignment = alignment,
				BackgroundColor = UIColor.Clear
			};
		}
	}
}