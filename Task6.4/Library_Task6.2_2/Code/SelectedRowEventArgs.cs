﻿using System;

namespace Task6
{
	public class SelectedRowEventArgs : EventArgs
	{
		private int selectedRowId;

		public int RowId 
		{ 
			get { return selectedRowId; }
			set { selectedRowId = value; }
		}

		public SelectedRowEventArgs(int selectedRowId)
		{
			this.selectedRowId = selectedRowId;
		}
	}
}