﻿using UIKit;
using System;
using System.Collections.Generic;

namespace Task6
{
public class EmployeeManager : IManager
	{
		public List<Employee> Employees { get; set; }

		public EmployeeManager()
		{
			Employees = new List<Employee>
			{
				new Employee()
				{
					Id = 1,
					FirstName = "John",
					LastName = "Doe",
					Position = "iOS Developer",
					HiringDate = new DateTime(2016, 12, 25),
					PhoneNumber = "+380457894132",
					Photo = "Avatars/employee1.jpeg"
				},
				new Employee()
				{
					Id = 2,
					FirstName = "Rob",
					LastName = "Martin",
					Position = "Android Developer",
					HiringDate = new DateTime(2015, 11, 24),
					PhoneNumber = "+38098741135",
					Photo = "Avatars/employee2.jpg"
				},
				new Employee()
				{
					Id = 3,
					FirstName = "Mike",
					LastName = "Ramsey",
					Position = ".NET Developer",
					HiringDate = new DateTime(2014, 10, 23),
					PhoneNumber = "+380675135477",
					Photo = "Avatars/employee3.gif"
				}
			};
		}

		public void Save(Employee employee)
		{
			if (employee.Id != -1)
				return;

			employee.Id = Employees.Count != 0 ?
				employee.Id = Employees[Employees.Count - 1].Id + 1 : employee.Id = 1;

			Employees.Add(employee);
		}

		public void Add(UIViewController controller)
		{
			int newEmployeeId = -1;
			var newEmployee = new Employee()
			{
				Id = newEmployeeId,
				HiringDate = DateTime.Now,
				Photo = "Avatars/newEmployee.png"
			};

			var detailVC = new DetailViewController();
			detailVC.SetEmployee(this, newEmployee);
			controller.NavigationController.PushViewController(detailVC, true);
		}

		public void Delete(UIViewController controller, Employee employee)
		{
			var oldEmployee = Employees.Find(t => t.Id == employee.Id);
			Employees.Remove(oldEmployee);
			controller.NavigationController.PopViewController(true);
		}
	}
}