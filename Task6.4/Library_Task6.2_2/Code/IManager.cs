﻿using UIKit;
using System.Collections.Generic;

namespace Task6
{
	public interface IManager
	{
		List<Employee> Employees
		{
			get;
			set;
		}

		void Save(Employee employee);
		void Add(UIViewController controller);
		void Delete(UIViewController controller, Employee employee);
	}
}