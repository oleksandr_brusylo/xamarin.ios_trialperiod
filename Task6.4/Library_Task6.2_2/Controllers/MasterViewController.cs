﻿using UIKit;

namespace Task6
{
	public partial class MasterViewController : UIViewController
	{
		IManager Manager;
		DetailViewController detailVC;
		RootTableSource source;

		public MasterViewController()
		{
			Title = "Task6.2_2";
			Manager = new EmployeeManager();
			detailVC = new DetailViewController();

			NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Add, (sender, args) => 
			{
				Manager.Add(this);
			}), true);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			source = new RootTableSource(Manager.Employees, this, detailVC);
			TableViewMasterVC.Source = source;

			source.UserSelectsRow += SetSelectedRowId;
		}

		private void SetSelectedRowId(object sender, SelectedRowEventArgs args)
		{
			var employee = source.GetItem(args.RowId);
			detailVC.SetEmployee(Manager, employee);
		}
	}
}