// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6
{
	[Register ("DetailViewController")]
	partial class DetailViewController
	{
		[Outlet]
		UIKit.UIImageView ImageViewPhoto { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldFirstName { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldHiringDate { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldLastName { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldPhoneNumber { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldPosition { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ImageViewPhoto != null) {
				ImageViewPhoto.Dispose ();
				ImageViewPhoto = null;
			}

			if (TextFieldFirstName != null) {
				TextFieldFirstName.Dispose ();
				TextFieldFirstName = null;
			}

			if (TextFieldLastName != null) {
				TextFieldLastName.Dispose ();
				TextFieldLastName = null;
			}

			if (TextFieldPosition != null) {
				TextFieldPosition.Dispose ();
				TextFieldPosition = null;
			}

			if (TextFieldHiringDate != null) {
				TextFieldHiringDate.Dispose ();
				TextFieldHiringDate = null;
			}

			if (TextFieldPhoneNumber != null) {
				TextFieldPhoneNumber.Dispose ();
				TextFieldPhoneNumber = null;
			}
		}
	}
}
