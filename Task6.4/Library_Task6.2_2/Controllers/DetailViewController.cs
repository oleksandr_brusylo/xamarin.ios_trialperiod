﻿using UIKit;

namespace Task6
{
	public partial class DetailViewController : UIViewController
	{
		UITextField[] textFields;
		UIBarButtonItem saveButton;
		Employee CurrentEmployee { get; set; }
		IManager Manager { get; set; }
		const int ButtonWidth = 50;
		string title = "Details";

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Title = title;
			ManageUIBarButtons();

			//loop through existing controls would be nicer but no success so far...
			textFields = new UITextField[]
			{
				TextFieldFirstName,
				TextFieldLastName,
				TextFieldPosition,
				TextFieldHiringDate,
				TextFieldPhoneNumber
			};
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Mapper.MapPropertiesToViews(textFields, CurrentEmployee);
			ImageViewPhoto.Image = UIImage.FromBundle(CurrentEmployee.Photo);
			ManageTextFields();
		}

		public void SetEmployee(IManager manager, Employee employee)
		{
			Manager = manager;
			CurrentEmployee = employee;
		}

		private void ManageTextFields()
		{
			var counter = 0;
			foreach (var textField in textFields)
			{
				if (counter == 0)
					textField.BecomeFirstResponder();
				textField.Layer.BorderColor = UIColor.White.CGColor;
				textField.ClearButtonMode = UITextFieldViewMode.WhileEditing;
			}
		}

		private void ManageUIBarButtons()
		{
			SetTrashRightBarButton();

			saveButton = new UIBarButtonItem(UIBarButtonSystemItem.Save);
			saveButton.Clicked += (sender, e) =>
			{
				if (InputValidator.InputIsValid(textFields, this))
				{
					Mapper.MapViewsToProperties(CurrentEmployee, textFields);
					Manager.Save(CurrentEmployee);
					NavigationController.PopViewController(true);
				}
			};
			SetBottomBarButtons();
		}

		private void SetTrashRightBarButton()
		{
			if (CurrentEmployee.Id != -1)
				NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(UIBarButtonSystemItem.Trash, (sender, args) =>
				{
					Manager.Delete(this, CurrentEmployee);
				}), true);
		}

		private void SetBottomBarButtons()
		{
			SetToolbarItems(new UIBarButtonItem[]
			{
				saveButton,
				new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace) { Width = ButtonWidth },
				new UIBarButtonItem(UIBarButtonSystemItem.Cancel, (s,e) =>
				{
					NavigationController.PopViewController(true);
				})
			}, false);
			NavigationController.ToolbarHidden = false;
		}
	}
}