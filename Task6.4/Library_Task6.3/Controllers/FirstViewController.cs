﻿using UIKit;

namespace Task6
{
	public partial class FirstViewController : BaseViewController
	{
		public FirstViewController() : base("FirstViewController", null)
		{
			title = "Yellow";
			mainColor = UIColor.FromRGB(253, 255, 146);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			SetAppearanceOfController(title, mainColor);
			SetRightBarButton<SecondViewController>();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			NavigationController.NavigationBar.BarTintColor = mainColor;
		}
	}
}