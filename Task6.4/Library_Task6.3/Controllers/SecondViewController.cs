﻿using UIKit;

namespace Task6
{
	public partial class SecondViewController : BaseViewController
	{
		public SecondViewController() : base("SecondViewController", null)
		{
			title = "Green";
			mainColor = UIColor.FromRGB(50, 177, 129);
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			SetAppearanceOfController(title, mainColor);
			SetRightBarButton<ThirdViewController>();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			NavigationController.NavigationBar.BarTintColor = mainColor;
		}
	}
}