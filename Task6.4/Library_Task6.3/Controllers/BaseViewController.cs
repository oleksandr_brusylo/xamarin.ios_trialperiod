﻿using UIKit;
using Foundation;

namespace Task6
{
	public class BaseViewController : UIViewController
	{
		protected string title;
		protected UIColor mainColor;
		protected UIBarButtonItem rightBarButton;
		private string rightBarButtonTitle = "Next page";

		public BaseViewController() : base ("BaseViewController", null)
		{}

		public BaseViewController(string nibName, NSBundle bundle) : base(nibName, bundle)
		{}

		public T NavigateTo<T>() where T : UIViewController, new()
		{
			var vc = new T();
			NavigationController.PushViewController(vc, true);
			return vc;
		}

		protected void SetAppearanceOfController(string title, UIColor mainColor)
		{
			Title = title;
			View.BackgroundColor = mainColor;
			NavigationController.NavigationBar.BarTintColor = mainColor;
		}

		protected void SetRightBarButton<T>() where T : UIViewController, new()
		{
			rightBarButton = new UIBarButtonItem();
			rightBarButton.Title = rightBarButtonTitle;
			NavigationItem.SetRightBarButtonItem(rightBarButton, true);
			rightBarButton.Clicked += (sender, e) => { NavigateTo<T>(); };
		}
	}
}