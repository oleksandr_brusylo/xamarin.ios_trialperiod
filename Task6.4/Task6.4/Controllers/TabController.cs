﻿using UIKit;

namespace Task6
{
	public class TabController : UITabBarController
	{
		UIViewController tab1, tab2, tab3;
		UINavigationController navControllerTabOne;
		UINavigationController navControllerTabTwo;
		UINavigationController navContorllerTabThree;
		FirstViewController firstVC;
		MasterViewController masterVC;
		AboutViewController aboutVC;

		public TabController()
		{
			firstVC = new FirstViewController();
			navControllerTabOne = new UINavigationController(firstVC);
			tab1 = navControllerTabOne;
			tab1.TabBarItem = new UITabBarItem(UITabBarSystemItem.MostViewed, 0);

			masterVC = new MasterViewController();
			navControllerTabTwo = new UINavigationController(masterVC);
			tab2 = navControllerTabTwo;
			tab2.TabBarItem = new UITabBarItem(UITabBarSystemItem.Contacts, 0);

			aboutVC = new AboutViewController();
			navContorllerTabThree = new UINavigationController(aboutVC);
			tab3 = navContorllerTabThree;
			tab3.TabBarItem = new UITabBarItem(UITabBarSystemItem.More, 0);

			var tabs = new UIViewController[] { tab1, tab2, tab3 };

			ViewControllers = tabs;
		}
	}
}