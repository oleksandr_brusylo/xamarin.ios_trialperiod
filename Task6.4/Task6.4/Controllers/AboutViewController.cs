﻿using UIKit;

namespace Task6
{
	public partial class AboutViewController : UIViewController
	{
		public AboutViewController() : base("AboutViewController", null)
		{}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			Title = "About application";
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
		}
	}
}