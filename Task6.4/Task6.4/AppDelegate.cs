﻿using Foundation;
using UIKit;

namespace Task6
{
	[Register("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate
	{
		public override UIWindow Window
		{
			get;
			set;
		}

		TabController tabController;

		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
			tabController = new TabController();
			Window.RootViewController = tabController;
			Window.MakeKeyAndVisible();
			return true;
		}

		public override void OnResignActivation(UIApplication application)
		{}

		public override void DidEnterBackground(UIApplication application)
		{}

		public override void WillEnterForeground(UIApplication application)
		{}

		public override void OnActivated(UIApplication application)
		{}

		public override void WillTerminate(UIApplication application)
		{}
	}
}