﻿using System;
using UIKit;

namespace Task6._2
{
	public static class Mapper
	{
		public static void MapPropertiesToViews(UITextField[] textFields, Employee employee)
		{
			textFields[0].Text = employee.FirstName;
			textFields[1].Text = employee.LastName;
			textFields[2].Text = employee.Position;
			textFields[3].Text = employee.HiringDate.ToShortDateString();
			textFields[4].Text = employee.PhoneNumber;
		}

		public static void MapViewsToProperties(Employee employee, UITextField[] textFields)
		{
			employee.FirstName = textFields[0].Text;
			employee.LastName = textFields[1].Text;
			employee.Position = textFields[2].Text;
			employee.HiringDate = DateTime.Parse(textFields[3].Text);
			employee.PhoneNumber = textFields[4].Text;
		}
	}
}