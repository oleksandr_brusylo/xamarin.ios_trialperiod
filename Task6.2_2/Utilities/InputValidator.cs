﻿using UIKit;
using System;

namespace Task6._2
{
	public static class InputValidator
	{
		public static bool InputIsValid(UITextField[] textFields, DetailViewController controller) 
		{
			SetEditingDidEndHandler(textFields, controller);

			bool inputIsValid = true;
			var incorrectTextFieldsCounter = 0;
			foreach (var textField in textFields)
			{
				textField.Text = textField.Text.Trim();
				if (textField.Text.Length <= 0)
				{
					controller.InvokeOnMainThread(() =>
					{
						textField.Layer.BorderColor = UIColor.Red.CGColor;
						textField.Layer.BorderWidth = 1;
						textField.Layer.CornerRadius = 5;
					});
					inputIsValid = false;
					if (incorrectTextFieldsCounter == 0)
						textField.BecomeFirstResponder();
					incorrectTextFieldsCounter++;
				}
			}
			return inputIsValid && DateIsValid(textFields[3], controller);
		}

		private static bool DateIsValid(UITextField hiringDate, DetailViewController controller) 
		{
			var tmp = new DateTime();
			var result = DateTime.TryParse(hiringDate.Text, out tmp);
			if (!result)
			{
				ShowIncorrectHiringDateFormatAlert(hiringDate, controller);
			}
			return result;
		}

		private static void SetEditingDidEndHandler(UITextField[] textFields, DetailViewController controller)
		{
			foreach (var textField in textFields)
			{
				if (textField.Placeholder.Equals("Hiring Date (mm/dd/yyyy)"))
				{
					continue;
				}
				textField.EditingDidEnd += (sender, e) => 
				{
					controller.InvokeOnMainThread (() => 
					{
						if(textField.Text.Length > 0)
						textField.Layer.BorderColor = UIColor.Clear.CGColor;
					});
				};
			}
		}

		public static void ShowIncorrectHiringDateFormatAlert(UITextField textField, DetailViewController controller)
		{
			textField.Layer.BorderColor = UIColor.Red.CGColor;
			textField.Layer.BorderWidth = 1;
			textField.Layer.CornerRadius = 5;
			textField.BecomeFirstResponder();

			var alert = UIAlertController.Create("Incorrect date format",
												 "Expected format is mm/dd/yyyy",
												 UIAlertControllerStyle.Alert);
			alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
			controller.PresentViewController(alert, true, null);
		}
	}
}