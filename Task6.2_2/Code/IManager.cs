﻿using UIKit;
using System;
using System.Collections.Generic;

namespace Task6._2
{
	public interface IManager
	{
		List<Employee> Employees 
		{
			get;
			set;
		}

		void Save(Employee employee);
		void Add(UIViewController controller);
		void Delete(UIViewController controller, Employee employee);
	}
}