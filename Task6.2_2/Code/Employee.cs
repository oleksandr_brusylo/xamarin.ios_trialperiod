﻿using System;

namespace Task6._2
{
	public class Employee
	{
		public int Id
		{ get; set; }

		public string FirstName
		{ get; set; }

		public string LastName
		{ get; set; }

		public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }

		public DateTime HiringDate
		{ get; set; }

		public string Position
		{ get; set; }

		public string PhoneNumber
		{ get; set; }

		public string Photo
		{ get; set; }
	}
}