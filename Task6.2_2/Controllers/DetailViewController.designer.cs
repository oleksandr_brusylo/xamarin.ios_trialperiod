// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Task6._2
{
	[Register ("TaskDetailViewController")]
	partial class DetailViewController
	{
		[Outlet]
		UIKit.UIButton DeleteButton { get; set; }

		[Outlet]
		UIKit.UITableView detail { get; set; }

		[Outlet]
		UIKit.UISwitch DoneSwitch { get; set; }

		[Outlet]
		UIKit.UIImageView ImageViewPhoto { get; set; }

		[Outlet]
		UIKit.UITextField NotesText { get; set; }

		[Outlet]
		UIKit.UIButton SaveButton { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldFirstName { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldHiringDate { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldLastName { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldPhoneNumber { get; set; }

		[Outlet]
		UIKit.UITextField TextFieldPosition { get; set; }

		[Outlet]
		UIKit.UITextField TitleText { get; set; }

		[Action ("DeleteButton_TouchUpInside:")]
		partial void DeleteButtonTouchUpInside (UIKit.UIButton sender);

		[Action ("SaveButton_TouchUpInside:")]
		partial void SaveButtonTouchUpInside (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ImageViewPhoto != null) {
				ImageViewPhoto.Dispose ();
				ImageViewPhoto = null;
			}

			if (DeleteButton != null) {
				DeleteButton.Dispose ();
				DeleteButton = null;
			}

			if (detail != null) {
				detail.Dispose ();
				detail = null;
			}

			if (DoneSwitch != null) {
				DoneSwitch.Dispose ();
				DoneSwitch = null;
			}

			if (NotesText != null) {
				NotesText.Dispose ();
				NotesText = null;
			}

			if (SaveButton != null) {
				SaveButton.Dispose ();
				SaveButton = null;
			}

			if (TextFieldFirstName != null) {
				TextFieldFirstName.Dispose ();
				TextFieldFirstName = null;
			}

			if (TextFieldHiringDate != null) {
				TextFieldHiringDate.Dispose ();
				TextFieldHiringDate = null;
			}

			if (TextFieldLastName != null) {
				TextFieldLastName.Dispose ();
				TextFieldLastName = null;
			}

			if (TextFieldPhoneNumber != null) {
				TextFieldPhoneNumber.Dispose ();
				TextFieldPhoneNumber = null;
			}

			if (TextFieldPosition != null) {
				TextFieldPosition.Dispose ();
				TextFieldPosition = null;
			}

			if (TitleText != null) {
				TitleText.Dispose ();
				TitleText = null;
			}
		}
	}
}
