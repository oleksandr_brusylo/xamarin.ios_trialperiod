using System;
using Foundation;
using UIKit;

namespace Task6._2
{
	public partial class MasterViewController : UITableViewController
	{
		IManager Manager { get; set; }

		public MasterViewController(IntPtr handle) : base(handle)
		{
			Title = "Task6.2_2";
			Manager = new EmployeeManager();
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier == "EmployeeSegue") 
			{ 
				var detailVC = segue.DestinationViewController as DetailViewController;
				if (detailVC != null) 
				{
					var source = TableView.Source as RootTableSource;
					var rowPath = TableView.IndexPathForSelectedRow;
					var employee = source.GetItem(rowPath.Row);
					detailVC.SetEmployee(Manager, employee);
				}
			}
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			TableView.Source = new RootTableSource(Manager.Employees, this);
		}

		partial void AddButton_Activated(UIBarButtonItem sender)
		{
			Manager.Add(this);
		}
	}
}